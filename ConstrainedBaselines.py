import copy
import random
from tensorflow.keras import backend as K
from tensorflow.keras.models import load_model

from Models import build_conv_network, get_policy, build_conv_network_entropy
from Utils import preprocess_data, predict_with_uncertainty
from ALILPE.Utils import simple_sampler_edit_distance, constrained_start

def run_entropy_sampling(ground_truth_dict, embedding_dict, name_to_seq, model_path = None, replicates = 10, initial_k = 10, budget = 10, n_seeds = 1):
    '''
    If model_path is specified, this is a warm start, otherwise, cold
    '''
    log_entropy = []  
    for rep in range(0, replicates):
        if model_path:
            model = load_model(model_path)
            initial_weights = model.get_weights()
        unlabeled_dictionary = copy.deepcopy(ground_truth_dict)
        unlabeled_seq_dict = copy.deepcopy(name_to_seq)
            
        #pretend we only have this many labeled data to start:
        #XXX: hardcoded min_connections below
        seeds = constrained_start(unlabeled_seq_dict, number_seeds = n_seeds, min_connections = 1)
        labeled_seq_dict = {}
        labeled_dictionary = {}
        for seed in seeds:
            labeled_seq_dict[seed] = unlabeled_seq_dict[seed]
            labeled_dictionary[seed] = unlabeled_dictionary[seed]
            del unlabeled_seq_dict[seed]
            del unlabeled_dictionary[seed]
            
        for n in range(initial_k - n_seeds):
            labels = simple_sampler_edit_distance(unlabeled_seq_dict, labeled_seq_dict, number_samples = 1)
            for label in labels:
                labeled_seq_dict[label] = unlabeled_seq_dict[label]
                labeled_dictionary[label] = unlabeled_dictionary[label]
                del unlabeled_seq_dict[label]
                del unlabeled_dictionary[label]

        x_train_full, y_train_full, trained_scaler = preprocess_data(labeled_dictionary, embedding_dict,\
            mode = 'labeled')
        if model_path:
            model.set_weights(initial_weights)
        else:
            model = build_conv_network_entropy(0.001, input_dims = 1900)
            initial_weights = model.get_weights()
        history = model.fit(x_train_full, y_train_full, batch_size = 64, epochs = 100, verbose = 0)
        loss_log = []
        loss = history.history['loss'][-1]
        loss_log.append(loss)
        for t in range(0, budget):
            #predict best
            
            connected_labels = simple_sampler_edit_distance(unlabeled_seq_dict, labeled_seq_dict, number_samples = 'all')
            working_dictionary = {}
            for label in connected_labels:
                working_dictionary[label] = unlabeled_dictionary[label]
            x_unlabeled, y_label = preprocess_data(working_dictionary, embedding_dict,\
            mode = 'unlabeled', scaler = trained_scaler)
            predictions, uncertainty = predict_with_uncertainty(model, x_unlabeled, num_iterations=100)
            indicies = uncertainty.argsort()[-1]
            best_label = y_label[indicies]  
            #add and retrain
            labeled_dictionary[best_label] = working_dictionary[best_label]
            del unlabeled_dictionary[best_label]
            labeled_seq_dict[best_label] = unlabeled_seq_dict[best_label]
            del unlabeled_seq_dict[best_label]
            
            x_train_full, y_train_full, trained_scaler = preprocess_data(labeled_dictionary, embedding_dict,\
            mode = 'labeled')
            model.set_weights(initial_weights)
            history = model.fit(x_train_full, y_train_full, batch_size = 64, epochs = 100, verbose = 0)
            loss = history.history['loss'][-1]
            loss_log.append(loss)
            
            #now predict the rest
            x_eval, y_eval = preprocess_data(unlabeled_dictionary, embedding_dict, mode = 'eval')
            predictions = model.predict(x_eval)
            log_entropy.append([rep, t, loss_log, list(labeled_dictionary.values()), predictions, y_eval])
        del model
        K.clear_session()
    return log_entropy
    
def run_random_sampling(ground_truth_dict, embedding_dict, name_to_seq, model_path = None, replicates = 10, initial_k = 10, budget = 10, n_seeds = 1):
    log_random = []
    for rep in range(0, replicates):
        if model_path:
            model = load_model(model_path)
            initial_weights = model.get_weights()
        unlabeled_dictionary = copy.deepcopy(ground_truth_dict)
        unlabeled_seq_dict = copy.deepcopy(name_to_seq)
        
        #pretend we only have this many labeled data to start:
        #XXX: hardcoded min_connections below
        seeds = constrained_start(unlabeled_seq_dict, number_seeds = n_seeds, min_connections = 1)
        labeled_seq_dict = {}
        labeled_dictionary = {}
        for seed in seeds:
            labeled_seq_dict[seed] = unlabeled_seq_dict[seed]
            labeled_dictionary[seed] = unlabeled_dictionary[seed]
            del unlabeled_seq_dict[seed]
            del unlabeled_dictionary[seed]

        for n in range(initial_k - n_seeds):
            labels = simple_sampler_edit_distance(unlabeled_seq_dict, labeled_seq_dict, number_samples = 1)
            for label in labels:
                labeled_seq_dict[label] = unlabeled_seq_dict[label]
                labeled_dictionary[label] = unlabeled_dictionary[label]
                del unlabeled_seq_dict[label]
                del unlabeled_dictionary[label]

        x_train_full, y_train_full, trained_scaler = preprocess_data(labeled_dictionary, embedding_dict,\
            mode = 'labeled')
        if model_path:
            model.set_weights(initial_weights)
        else:
            model = build_conv_network(0.001, input_dims = 1900)
        history = model.fit(x_train_full, y_train_full, batch_size = 64, epochs = 100, verbose = 0)
        loss = history.history['loss'][-1]
        loss_log = [loss]
        for t in range(0, budget):
            label = simple_sampler_edit_distance(unlabeled_seq_dict, labeled_seq_dict, number_samples = 1)[0]
            labeled_dictionary[label] = unlabeled_dictionary[label]
            del unlabeled_dictionary[label]
            labeled_seq_dict[label] = unlabeled_seq_dict[label]
            del unlabeled_seq_dict[label]

            x_train_full, y_train_full, trained_scaler = preprocess_data(labeled_dictionary, embedding_dict,\
            mode = 'labeled')
            history = model.fit(x_train_full, y_train_full, batch_size = 64, epochs = 100, verbose = 0)
            loss = history.history['loss'][-1]
            loss_log.append(loss)
            
            x_eval, y_eval = preprocess_data(unlabeled_dictionary, embedding_dict, mode = 'eval')
            predictions = model.predict(x_eval)
            log_random.append([rep, t, loss, list(labeled_dictionary.values()), predictions, y_eval])
        del model
        K.clear_session()
    return log_random

def run_greedy_sampling(ground_truth_dict, embedding_dict, name_to_seq, model_path = None, replicates = 10, initial_k = 10, budget = 10, direction = 'high', n_seeds = 1):
    log_greedy = []
    for rep in range(0, replicates):
        if model_path:
            model = load_model(model_path)
            initial_weights = model.get_weights()
        unlabeled_dictionary = copy.deepcopy(ground_truth_dict)
        unlabeled_seq_dict = copy.deepcopy(name_to_seq)
        
        #pretend we only have this many labeled data to start:
        #XXX: hardcoded min_connections below
        seeds = constrained_start(unlabeled_seq_dict, number_seeds = n_seeds, min_connections = 1)
        labeled_seq_dict = {}
        labeled_dictionary = {}
        for seed in seeds:
            labeled_seq_dict[seed] = unlabeled_seq_dict[seed]
            labeled_dictionary[seed] = unlabeled_dictionary[seed]
            del unlabeled_seq_dict[seed]
            del unlabeled_dictionary[seed]
            
        for n in range(initial_k - n_seeds):
            labels = simple_sampler_edit_distance(unlabeled_seq_dict, labeled_seq_dict, number_samples = 1)
            for label in labels:
                labeled_seq_dict[label] = unlabeled_seq_dict[label]
                labeled_dictionary[label] = unlabeled_dictionary[label]
                del unlabeled_seq_dict[label]
                del unlabeled_dictionary[label]

        x_train_full, y_train_full, trained_scaler = preprocess_data(labeled_dictionary, embedding_dict,\
            mode = 'labeled')
        if model_path:
            model.set_weights(initial_weights)
        else:
            model = build_conv_network(0.001, input_dims = 1900)
        history = model.fit(x_train_full, y_train_full, batch_size = 64, epochs = 100, verbose = 0)
        loss_log = []
        loss = history.history['loss'][-1]
        loss_log.append(loss)
        for t in range(0, budget):
            #predict best
            connected_labels = simple_sampler_edit_distance(unlabeled_seq_dict, labeled_seq_dict, number_samples = 'all')
            working_dictionary = {}
            for label in connected_labels:
                working_dictionary[label] = unlabeled_dictionary[label]

            x_unlabeled, y_label = preprocess_data(working_dictionary, embedding_dict,\
            mode = 'unlabeled', scaler = trained_scaler)
            predictions = model.predict(x_unlabeled)
            if direction == 'high':
                indicies = predictions.argsort()[-1][0]
            elif direction == 'low':
                indicies = predictions.argsort()[0][0]
            else:
                raise ValueError(f'Recieved an illegal value for direction: {direction}')
            best_label = y_label[indicies]
            #add and retrain
            labeled_dictionary[best_label] = unlabeled_dictionary[best_label]
            del unlabeled_dictionary[best_label]
            labeled_seq_dict[best_label] = unlabeled_seq_dict[best_label]
            del unlabeled_seq_dict[best_label]
            
            x_train_full, y_train_full, trained_scaler = preprocess_data(labeled_dictionary, embedding_dict,\
            mode = 'labeled')
            
            history = model.fit(x_train_full, y_train_full, batch_size = 64, epochs = 100, verbose = 0)
            loss = history.history['loss'][-1]
            loss_log.append(loss)
            #now predict the rest
            x_eval, y_eval = preprocess_data(unlabeled_dictionary, embedding_dict, mode = 'eval')
            predictions = model.predict(x_eval)
            log_greedy.append([rep, t, loss_log, list(labeled_dictionary.values()), predictions, y_eval])
        del model
        K.clear_session()
    return log_greedy
