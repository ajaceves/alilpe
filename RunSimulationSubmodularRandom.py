import random
import pickle
import glob
import os
import copy

import numpy as np

import tensorflow as tf
import argparse
from tensorflow import keras
from tensorflow.keras.models import Model, load_model, Sequential
from tensorflow.keras import backend as K
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.callbacks import EarlyStopping
from Utils import init_logger, load_dataset, get_run_name
from ALILPE.Models import build_conv_network, get_policy_submodular, SubmodularModel
from ALILPE.Utils import init_logger, load_dataset, get_run_name, preprocess_data, get_a_state, get_all_states_w_super

###############################################################################################
parser = argparse.ArgumentParser(description='Train a Policy with ALIL')
parser.add_argument('--train_data', default='/home/aiden/Documents/Mayo/ActiveLearning/datasets/ProtaBank_3xESLyS9/', help='data set to run tests on')
parser.add_argument('--budget', default=100, help='number of data to add to model')
parser.add_argument('--episodes', default=20, help='number of episodes (replicates)')
parser.add_argument('--n_seeds', default=1, help='number of initally labeled data to constrain edit distance')
parser.add_argument('--initial_k', default=10, help='number of data to seed the model with')
parser.add_argument('--initial_split', default=100, help='number of data to use for evaluating progress')
parser.add_argument('--k_num', default=30, help='number of data to consider (pool size)')
#parser.add_argument('--num_super', default=30, help='number of random data to add to superhistory -- later add random and as % of true labeled')
parser.add_argument('--lam', default=1., help='lambda for blended loss')
args = parser.parse_args()

# parse
###############################################################################################
train_data = args.train_data
budget = args.budget
episodes = args.episodes
initial_k = args.initial_k
initial_split = args.initial_split
k_num = args.k_num
n_seeds = args.n_seeds
#num_super = args.num_super
lam = float(args.lam)
###############################################################################################

physical_devices = tf.config.experimental.list_physical_devices('GPU')
assert len(physical_devices) > 0, "Not enough GPU hardware devices available"
tf.config.experimental.set_memory_growth(physical_devices[0], True)

host, stamp = get_run_name()
os.mkdir('models_and_policies/run_' + stamp)
    
run_name = 'models_and_policies/run_' + stamp + '/RunSimulation_' + host + '_' + stamp 
with open(run_name + '_args.txt', 'w') as handle:
    handle.write(str(args))

logger = init_logger(log_file = run_name + '.log')

ground_truth_dict, embedding_dict, name_to_seq = load_dataset(train_data)

#main code
###############################################################################################
policy_name = run_name + '_policy.ckpt'
classifier_name = run_name + '_classifier.ckpt'
optimizer = Adam(lr=1e-3)
policy = SubmodularModel(get_policy_submodular(k_num, 258), lam)
policy.compile(optimizer=optimizer)

states = []
superstates = []
actions = []

logger.info('Begin training AL policy:')
for tau in range(0, episodes):
    logger.info(f' * Start episode {tau}')
    logger.info("Split data into train and validation:")
    #XXX: Validation is not as constrained as our sampling will be
    unlabeled_dictionary = copy.deepcopy(ground_truth_dict)
    labels = random.sample(unlabeled_dictionary.keys(), k = initial_split)
    validation_dictionary = {}
    for label in labels:
        validation_dictionary[label] = unlabeled_dictionary[label]
        del unlabeled_dictionary[label]
    
    #remove validation data from seq dict
    unlabeled_seq_dict = copy.deepcopy(name_to_seq)
    for label in labels:
        del unlabeled_seq_dict[label]
        
    logger.info("Seed experiment with initial labeling:")
    labels = random.sample(unlabeled_dictionary.keys(), k = initial_k)
    labeled_dictionary = {}
    labeled_seq_dict = {}
    for label in labels:
        labeled_dictionary[label] = unlabeled_dictionary[label]
        labeled_seq_dict[label] = unlabeled_seq_dict[label]
        del unlabeled_seq_dict[label]
        del unlabeled_dictionary[label]
        
    #if tau > 0:
    #    logger.info(f'[Episode {tau}] Load Policy from path {policy_name}')
    #    policy = load_model(policy_name)

    model = build_conv_network(0.001, input_dims = 1900)
    initial_weights = model.get_weights()
    x_train, y_train, scaler = preprocess_data(labeled_dictionary, embedding_dict, mode = 'labeled')
    model.fit(np.array(x_train), y_train, epochs = 100, verbose = 0)
    current_weights = model.get_weights()
    model.save(classifier_name)

    #toss a coin
    coin = np.random.rand(1)
    if tau == 0:
        coin = 0
    for t in range(0, budget):
        x_sampled = []
        y_sampled = []
        loss = 1e10
        row = 0
        best_index = 0
        num_super = len(labeled_dictionary.keys())
        trajectory_and_super_labels = random.sample(unlabeled_dictionary.keys(), k = k_num + num_super)
        trajectory_unlabeled_x = []
        x_super = []
        for label in trajectory_and_super_labels[:k_num]:
            trajectory_unlabeled_x.append(embedding_dict[label])
        trajectory_unlabeled_x = np.array(trajectory_unlabeled_x)
        
        for label in trajectory_and_super_labels[k_num:]:
            x_super.append(embedding_dict[label])
        x_super = np.array(x_super)
        
        x_train_full, y_train_full, trained_scaler = preprocess_data(labeled_dictionary, embedding_dict,\
            mode = 'labeled', force_zero = True)
        x_train_full = np.array(x_train_full)
        y_train_full = np.array(y_train_full)

        state, superstate = get_all_states_w_super(x_train_full, y_train_full, trajectory_unlabeled_x, x_super, model)
        
        if ( coin > 0.5 ):
            logger.info(f'* Coin returned {coin}, using POLICY')
            #no need to do rollout!
            temp_states = np.expand_dims(state, axis = 0)
            all_actions = policy.predict(temp_states)
            action = np.argmax(all_actions)
            logger.info(f'Episode:{(tau+1)} Budget:{(t+1)}')
        else:
            logger.info(f'* Coin returned {coin}, using EXPERT')
            #rollout
            x_val, y_val, _scaler = preprocess_data(validation_dictionary, embedding_dict,\
                    mode = 'labeled', scaler = trained_scaler)
            x_val = np.array(x_val)
            y_val = np.array(y_val)
            
            for i, single_label in enumerate(trajectory_and_super_labels[:k_num]):
                y_temp_raw =  { single_label : unlabeled_dictionary[single_label] }
                #should we train on whole set + 1? or just the one point?
                x_train_temp, y_train_temp, _scaler = preprocess_data(y_temp_raw, embedding_dict,\
                    mode = 'labeled', scaler = trained_scaler)

                model.set_weights(current_weights)
                x_train_temp = np.array(x_train_temp)
                y_train_temp = np.array(y_train_temp)

                history = model.fit(x_train_temp, y_train_temp,\
                    validation_data = (x_val, y_val), epochs = 1, verbose = 0) #Changed to 1 epoch here
                val_loss = history.history['val_loss'][-1]
                #if we improved over previous best, notate metric and index
                if ( val_loss < loss ):
                    best_index = i #this was where the error was
                    loss = val_loss
            #end rollout
            logger.info(f'Episode:{(tau+1)} Budget:{(t+1)} Best index: {best_index}')
            action = best_index
        
        states.append(state)
        superstates.append(superstate)

        actions.append(action)
        
        selected_label = trajectory_and_super_labels[action]
        #adding the data...
        labeled_dictionary[selected_label] = ground_truth_dict[selected_label]
        del unlabeled_dictionary[selected_label]
        labeled_seq_dict[selected_label] = unlabeled_seq_dict[selected_label]
        del unlabeled_seq_dict[selected_label]

        
        model.set_weights(initial_weights)
        x_train, y_train, scaler = preprocess_data(labeled_dictionary, embedding_dict,\
            mode = 'labeled')
            
        #need to reselect network to take into account training on ONE data
        #this is bad. it is setting weights to the random initialized weights, then only training for one epoch! then that model is used by policy as states in the next iteration
        model.fit(np.array(x_train), y_train, epochs = 100, verbose = 0) #changed to 100 epochs 4-15
        current_weights = model.get_weights()
    model.save(classifier_name)
        
    logger.info(f'* End of episode, training policy network using replay memory')

    current_states = np.array(states)
    current_superstates = np.array(superstates)
    current_actions = to_categorical(np.asarray(actions), num_classes=k_num)
    ES = EarlyStopping(monitor='val_loss', min_delta=0.001, patience=20, verbose=0, mode='min')
    train_history = policy.fit([current_states, current_superstates], current_actions, verbose = 0, epochs = 1000, validation_split = 0.2, callbacks = [ES])
        
    logger.info(f'* End episode {tau}. Saving policy to {policy_name}')
    policy.save(policy_name)
    K.clear_session()
    del model
pickle.dump(states, open(run_name + '_states.pickle', 'wb'))
pickle.dump(actions, open(run_name + '_actions.pickle', 'wb'))
