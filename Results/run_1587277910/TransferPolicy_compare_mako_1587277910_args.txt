Namespace(budget=100, cycles=10, direction='low', dream_budget=10, initial_k=10, k_num=30, model='/home/aiden/Documents/Mayo/ActiveLearning/ALILPE/models_and_policies/run_1586987471/RunSimulation_mako_1586987471_classifier.h5', n_dreams=5, policy='/home/aiden/Documents/Mayo/ActiveLearning/ALILPE/models_and_policies/run_1586987471/RunSimulation_mako_1586987471_policy.h5', replicates=10, runs=['cold_dream', 'warm_dream'], test_data='/home/aiden/Documents/Mayo/ActiveLearning/datasets/ProtaBank_pfN5Rjzn/seperate_chains/heavy/', uncertainty_sample=False)

def preprocess_data(data_labels, state_space, mode='labeled', scaler = None):
    '''
    mode can be labeled, unlabeled, or eval
    
    THIS VERSION DOES NOT INVERT LABELS, OR LOG TRANSFORM
    '''
    if mode == 'labeled':
        ids = [variant for variant, label in data_labels.items() if label]

        x = np.array([state_space[v] for v in ids])
        y = np.array([data_labels[v] for v in ids]).reshape(-1,1)
        if not scaler:
            scaler = MinMaxScaler()
            y = scaler.fit_transform(y)
        else:
            y = scaler.transform(y)
        
        return x, y, scaler
    
    elif mode == 'unlabeled':
        unlabeled_ids = [variant for variant, label in data_labels.items() if label]
        x_unlabeled = np.array([state_space[v] for v in unlabeled_ids])
        return x_unlabeled, unlabeled_ids
    
    elif mode == 'eval':
        test_ids = [variant for variant, label in data_labels.items()]
        x_test = np.array([state_space[v] for v in test_ids])
        y_test = np.array([data_labels[v] for v in test_ids])
        
        return x_test, y_test
    
    else:
        raise ValueError('Invalid mode provided')


def run_ALIL_transfer(ground_truth_dict, embedding_dict, policy_path, model_path = None, replicates = 10, initial_k = 10, budget = 10, k_num = 10):
    policy = load_model(policy_path)
    log_ALIL = []
    if model_path:
        model = load_model(model_path)
    else:
        model = build_conv_network(0.001, input_dims = 1900)
    for rep in range(0, replicates):
        unlabeled_dictionary = copy.deepcopy(ground_truth_dict)
        labeled_dictionary = {}
        loss_log = []
        
        #start with a random sample    
        if initial_k:
            labels = random.sample(unlabeled_dictionary.keys(), k = initial_k)
            for label in labels:
                labeled_dictionary[label] = unlabeled_dictionary[label]
                del unlabeled_dictionary[label]

        #added this 4-15
        x_train, y_train, scaler = preprocess_data(labeled_dictionary, embedding_dict,\
                mode = 'labeled')

        history = model.fit(np.array(x_train), y_train, epochs = 100, verbose = 0)
        loss = history.history['loss'][-1]
        loss_log.append(loss)
        initial_weights = model.get_weights()
        #end added this 4-15
        
        for t in range(0, budget):
            if k_num > len(unlabeled_dictionary.keys()):
                trajectory_labels = list(unlabeled_dictionary.keys())
            else:
                trajectory_labels = random.sample(unlabeled_dictionary.keys(), k = k_num)
            trajectory_unlabeled_x = []
            
            for label in trajectory_labels:
                trajectory_unlabeled_x.append(embedding_dict[label])
            trajectory_unlabeled_x = np.array(trajectory_unlabeled_x)

            x_train_full, y_train_full, trained_scaler = preprocess_data(labeled_dictionary, embedding_dict,\
                mode = 'labeled')
            state = get_all_states(x_train_full, y_train_full, trajectory_unlabeled_x, model)
            
            temp_states = np.expand_dims(state, axis = 0)
            all_actions = policy.predict(temp_states)
            action = np.argmax(all_actions)

            selected_label = trajectory_labels[action]
            #adding the data...
            labeled_dictionary[selected_label] = ground_truth_dict[selected_label]
            del unlabeled_dictionary[selected_label]
            model.set_weights(initial_weights)

            x_train, y_train, scaler = preprocess_data(labeled_dictionary, embedding_dict,\
                mode = 'labeled')

            history = model.fit(np.array(x_train), y_train, epochs = 100, verbose = 0)
            loss = history.history['loss'][-1]
            loss_log.append(loss)
            #now predict the rest
            x_eval, y_eval = preprocess_data(unlabeled_dictionary, embedding_dict, mode = 'eval')
            predictions = model.predict(x_eval)
            log_ALIL.append([rep, t, loss_log, list(labeled_dictionary.values()), predictions, y_eval])
    del model
    K.clear_session()
    return log_ALIL


def run_ALIL_dreaming(ground_truth_dict, embedding_dict, policy_path, model_path = None, replicates = 10, initial_k = 10, budget = 100, k_num = 30, cycles = 10, number_dreams = 5, dream_budget = 10, initial_split = 5, uncertainty_sample = True):
    #TODO: start with just dreaming, then add the entropy based sampling technique in
    #TODO: this version does not have outer loop which is the so called "timesteps". is this the same thing as replicates?
    wake_length = budget // cycles
    policy = load_model(policy_path) #policy weights should persist, and be retrained on all actions and states at the end of each replicate
    log_ALIL = []
    if model_path:
        model = load_model(model_path)
    else:
        model = build_conv_network(0.001, input_dims = 1900)     
    
    initial_weights = model.get_weights()

    for rep in range(0, replicates):
        unlabeled_dictionary = copy.deepcopy(ground_truth_dict)
        labeled_dictionary = {}
        loss_log = []

        #start with a random sample #TODO Fix this, so you can start without it?
        if initial_k:
            labels = random.sample(unlabeled_dictionary.keys(), k = initial_k)
            for label in labels:
                labeled_dictionary[label] = unlabeled_dictionary[label]
                del unlabeled_dictionary[label]

        x_train, y_train, scaler = preprocess_data(labeled_dictionary, embedding_dict,\
                mode = 'labeled')

        history = model.fit(np.array(x_train), y_train, epochs = 100, verbose = 0)
        running_weights = model.get_weights()
        loss = history.history['loss'][-1]
        loss_log.append(loss)

        for t in range(0, cycles):                        
            model.set_weights(running_weights)
            #learn cycle
            for tw in range(wake_length):
            
                if uncertainty_sample:
                    x_unlabeled, unlabeled_ids = preprocess_data(unlabeled_dictionary, embedding_dict,\
                    mode = 'unlabeled', scaler = scaler)
                    predictions, uncertainty = predict_with_uncertainty(model, x_unlabeled, num_iterations=100)
                    indicies = uncertainty.argsort()[-(k_num*10):]
                    high_entropy_pool = list(np.take(unlabeled_ids, indicies))
                    trajectory_labels = random.sample(high_entropy_pool, k = k_num) #XXX: does not handle running out of data
                    
                else:
                #draw a random sample
                    if k_num > len(unlabeled_dictionary.keys()):
                        trajectory_labels = list(unlabeled_dictionary.keys())
                    else:
                        trajectory_labels = random.sample(unlabeled_dictionary.keys(), k = k_num)
                
                trajectory_unlabeled_x = []
                for label in trajectory_labels:
                    trajectory_unlabeled_x.append(embedding_dict[label])
                trajectory_unlabeled_x = np.array(trajectory_unlabeled_x)
                    #done with random sample
                
                x_train_full, y_train_full, trained_scaler = preprocess_data(labeled_dictionary, embedding_dict,\
                    mode = 'labeled')
                state = get_all_states(x_train_full, y_train_full, trajectory_unlabeled_x, model)

                temp_states = np.expand_dims(state, axis = 0)
                all_actions = policy.predict(temp_states)
                action = np.argmax(all_actions)

                selected_label = trajectory_labels[action]
                #adding the data...
                labeled_dictionary[selected_label] = ground_truth_dict[selected_label]
                del unlabeled_dictionary[selected_label]
                #TODO: think about these weights. the policy looks at the model weights, so does it really make sense to be resetting these here?
                model.set_weights(initial_weights)
                x_train, y_train, scaler = preprocess_data(labeled_dictionary, embedding_dict,\
                    mode = 'labeled')

                history = model.fit(np.array(x_train), y_train, epochs = 100, verbose = 0)
                loss = history.history['loss'][-1]
                loss_log.append(loss)
                #now predict the rest
                x_eval, y_eval = preprocess_data(unlabeled_dictionary, embedding_dict, mode = 'eval')
                predictions = model.predict(x_eval)
                log_ALIL.append([rep, t, loss_log, list(labeled_dictionary.values()), predictions, y_eval])
                #paper returns model, step, accuracy, train, pool
            #end learn cycle
            running_weights = model.get_weights()
            
            if (t+1) == cycles:
                continue

            #dream cycle
            states = []
            actions = []
            for tau in range(0, number_dreams):
                dream_train_dict = copy.deepcopy(labeled_dictionary)
                validation_size = len(labeled_dictionary) // 4
                dream_unlabeled_dict = copy.deepcopy(unlabeled_dictionary)
                labels = random.sample(dream_train_dict.keys(), k = validation_size)
                validation_dictionary = {}
                for label in labels:
                    validation_dictionary[label] = dream_train_dict[label]
                    del dream_train_dict[label]

                model.set_weights(initial_weights)
                x_train, y_train, scaler = preprocess_data(dream_train_dict, embedding_dict, mode = 'labeled')
                model.fit(np.array(x_train), y_train, epochs = 100, verbose = 0)
                dream_weights = model.get_weights()

                #toss a coin
                for t in range(0, dream_budget):
                    #draw a random sample
                    if k_num > len(dream_unlabeled_dict.keys()):
                        trajectory_labels = list(dream_unlabeled_dict.keys())
                    else:
                        trajectory_labels = random.sample(dream_unlabeled_dict.keys(), k = k_num)
                    trajectory_unlabeled_x = []
                    for label in trajectory_labels:
                        trajectory_unlabeled_x.append(embedding_dict[label])
                    trajectory_unlabeled_x = np.array(trajectory_unlabeled_x)
                    #done with random sample
                
                    x_sampled = []
                    y_sampled = []
                    loss = 1e10
                    row = 0
                    best_index = 0
                    
                    x_train_full, y_train_full, trained_scaler = preprocess_data(dream_train_dict, embedding_dict,\
                        mode = 'labeled')
                    x_train_full = np.array(x_train_full)
                    y_train_full = np.array(y_train_full)
                    state = get_all_states(x_train_full, y_train_full, trajectory_unlabeled_x, model)
                    
                    #construct pseudo-ground truth dict!
                    y_traj_predicted = model.predict(trajectory_unlabeled_x)
                    
                    coin = np.random.rand(1)
                    if ( coin > 0.5 ):
                        temp_states = np.expand_dims(state, axis = 0)
                        all_actions = policy.predict(temp_states)
                        action = np.argmax(all_actions)
                    else:
                        #rollout
                        x_val, y_val, _scaler = preprocess_data(validation_dictionary, embedding_dict,\
                                mode = 'labeled', scaler = trained_scaler)
                        x_val = np.array(x_val)
                        y_val = np.array(y_val)
                        
                        for i, single_label in enumerate(trajectory_labels):
                            y_temp_raw =  { single_label : y_traj_predicted[i] }

                            x_train_temp, y_train_temp, _scaler = preprocess_data(y_temp_raw, embedding_dict,\
                                mode = 'labeled', scaler = trained_scaler)

                            model.set_weights(dream_weights)
                            history = model.fit(x_train_temp, y_train_temp,\
                                validation_data = (x_val, y_val), epochs = 1, verbose = 0)
                            val_loss = history.history['val_loss'][-1]
                            #if we improved over previous best, notate metric and index
                            if ( val_loss < loss ):
                                best_index = i
                                loss = val_loss
                        #end rollout
                        action = best_index

                    states.append(state)
                    actions.append(action)

                    selected_label = trajectory_labels[action]
                    #adding the data...
                    dream_train_dict[selected_label] = ground_truth_dict[selected_label]
                    del dream_unlabeled_dict[selected_label]

                    model.set_weights(initial_weights)
                    x_train, y_train, scaler = preprocess_data(dream_train_dict, embedding_dict,\
                        mode = 'labeled')
                        
                    #need to reselect network to take into account training on ONE data 
                    model.fit(np.array(x_train), y_train, epochs = 100, verbose = 0)
                    dream_weights = model.get_weights()
                    
                current_states = np.array(states)
                current_actions = to_categorical(np.asarray(actions), num_classes=k_num)
                ES = EarlyStopping(monitor='val_accuracy', min_delta=0.001, patience=20, verbose=0, mode='max')
                train_history = policy.fit(current_states, current_actions, verbose = 0, epochs = 1000, validation_split = 0.2, callbacks = [ES])
            #end dream cycle
        #end cycle
    #end replicate
    del model
    K.clear_session()
    return log_ALIL
