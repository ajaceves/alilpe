Namespace(budget=100, direction='low', initial_k=10, k_num=30, model='/home/aiden/Documents/Mayo/ActiveLearning/ALILPE/models_and_policies/run_1586203622/RunSimulation_mako_1586203622_classifier.h5', policy='/home/aiden/Documents/Mayo/ActiveLearning/ALILPE/models_and_policies/run_1586203622/RunSimulation_mako_1586203622_policy.h5', replicates=10, runs=['ALIL_warm', 'ALIL_cold', 'random_warm', 'random_cold', 'uncertainty_warm', 'uncertainty_cold', 'greedy_warm', 'greedy_cold'], test_data='/home/aiden/Documents/Mayo/ActiveLearning/datasets/ProtaBank_pfN5Rjzn/seperate_chains/heavy/')

def preprocess_data(data_labels, state_space, mode='labeled', scaler = None):
    '''
    mode can be labeled, unlabeled, or eval
    
    THIS VERSION DOES NOT INVERT LABELS, OR LOG TRANSFORM
    '''
    if mode == 'labeled':
        ids = [variant for variant, label in data_labels.items() if label]

        x = np.array([state_space[v] for v in ids])
        y = np.array([data_labels[v] for v in ids]).reshape(-1,1)
        if not scaler:
            scaler = MinMaxScaler()
            y = scaler.fit_transform(y)
        else:
            y = scaler.transform(y)
        
        return x, y, scaler
    
    elif mode == 'unlabeled':
        unlabeled_ids = [variant for variant, label in data_labels.items() if label]
        x_unlabeled = np.array([state_space[v] for v in unlabeled_ids])
        return x_unlabeled, unlabeled_ids
    
    elif mode == 'eval':
        test_ids = [variant for variant, label in data_labels.items()]
        x_test = np.array([state_space[v] for v in test_ids])
        y_test = np.array([data_labels[v] for v in test_ids])
        
        return x_test, y_test
    
    else:
        raise ValueError('Invalid mode provided')


def run_ALIL_transfer(ground_truth_dict, embedding_dict, policy_path, model_path = None, replicates = 10, initial_k = 10, budget = 10, k_num = 10):
    policy = load_model(policy_path)
    log_ALIL = []
    if model_path:
        model = load_model(model_path)
        initial_weights = model.get_weights()
    else:
        model = build_conv_network(0.001, input_dims = 1900)     
        initial_weights = model.get_weights()    
    for rep in range(0, replicates):
        unlabeled_dictionary = copy.deepcopy(ground_truth_dict)
        labeled_dictionary = {}
        loss_log = []
        
        #start with a random sample    
        if initial_k:
            labels = random.sample(unlabeled_dictionary.keys(), k = initial_k)
            for label in labels:
                labeled_dictionary[label] = unlabeled_dictionary[label]
                del unlabeled_dictionary[label]
                
        for t in range(0, budget):
            if k_num > len(unlabeled_dictionary.keys()):
                trajectory_labels = list(unlabeled_dictionary.keys())
            else:
                trajectory_labels = random.sample(unlabeled_dictionary.keys(), k = k_num)
            trajectory_unlabeled_x = []
            
            for label in trajectory_labels:
                trajectory_unlabeled_x.append(embedding_dict[label])
            trajectory_unlabeled_x = np.array(trajectory_unlabeled_x)

            x_train_full, y_train_full, trained_scaler = preprocess_data(labeled_dictionary, embedding_dict,\
                mode = 'labeled')
            state = get_all_states(x_train_full, y_train_full, trajectory_unlabeled_x, model)
            
            temp_states = np.expand_dims(state, axis = 0)
            all_actions = policy.predict(temp_states)
            action = np.argmax(all_actions)

            selected_label = trajectory_labels[action]
            #adding the data...
            labeled_dictionary[selected_label] = ground_truth_dict[selected_label]
            del unlabeled_dictionary[selected_label]
            model.set_weights(initial_weights)

            x_train, y_train, scaler = preprocess_data(labeled_dictionary, embedding_dict,\
                mode = 'labeled')

            history = model.fit(np.array(x_train), y_train, epochs = 100, verbose = 0)
            loss = history.history['loss'][-1]
            loss_log.append(loss)
            #now predict the rest
            x_eval, y_eval = preprocess_data(unlabeled_dictionary, embedding_dict, mode = 'eval')
            predictions = model.predict(x_eval)
            log_ALIL.append([rep, budget, loss_log, list(labeled_dictionary.values()), predictions, y_eval])
    del model
    K.clear_session()
    return log_ALIL
