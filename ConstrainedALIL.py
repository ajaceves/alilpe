import copy
import random
import numpy as np
from tensorflow.keras import backend as K
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.models import load_model
from tensorflow.keras.callbacks import EarlyStopping

from Models import build_conv_network, get_policy, build_simple_network
from Utils import preprocess_data, get_all_states, predict_with_uncertainty
from ALILPE.Utils import simple_sampler_edit_distance, constrained_start

def run_ALIL_transfer(ground_truth_dict, embedding_dict, name_to_seq, policy_path, model_path = None, replicates = 10, initial_k = 10, budget = 10, k_num = 10, parallel_label = 1, n_seeds = 1, uncertainty_sample = True):
    policy = load_model(policy_path)
    log_ALIL = []
    if model_path:
        model = load_model(model_path)
    else:
        model = build_conv_network(0.001, input_dims = 1900)
    for rep in range(0, replicates):
        unlabeled_dictionary = copy.deepcopy(ground_truth_dict)
        unlabeled_seq_dict = copy.deepcopy(name_to_seq)
        labeled_dictionary = {}
        loss_log = []
        
        #pretend we only have this many labeled data to start:
        seeds = constrained_start(unlabeled_seq_dict, number_seeds = n_seeds, min_connections = k_num)
        labeled_seq_dict = {}
        labeled_dictionary = {}
        for seed in seeds:
            labeled_seq_dict[seed] = unlabeled_seq_dict[seed]
            labeled_dictionary[seed] = unlabeled_dictionary[seed]
            del unlabeled_seq_dict[seed]
            del unlabeled_dictionary[seed]

        for n in range(initial_k - n_seeds):
            labels = simple_sampler_edit_distance(unlabeled_seq_dict, labeled_seq_dict, number_samples = 1)
            for label in labels:
                labeled_seq_dict[label] = unlabeled_seq_dict[label]
                labeled_dictionary[label] = unlabeled_dictionary[label]
                del unlabeled_seq_dict[label]
                del unlabeled_dictionary[label]

        #resume updating here
        #added this 4-15
        x_train, y_train, scaler = preprocess_data(labeled_dictionary, embedding_dict,\
                mode = 'labeled')

        history = model.fit(np.array(x_train), y_train, epochs = 100, verbose = 0)
        loss = history.history['loss'][-1]
        loss_log.append(loss)
        initial_weights = model.get_weights()
        #end added this 4-15
        
        for t in range(0, budget):
            temp_dict = {}
            temp_seq_dict = {}
            for n in range(parallel_label):
                if k_num > len(unlabeled_dictionary.keys()):
                    trajectory_labels = list(unlabeled_dictionary.keys())
                else:
                    if uncertainty_sample:
                        connected_labels = simple_sampler_edit_distance(unlabeled_seq_dict, labeled_seq_dict, number_samples = 'all')
                        working_dictionary = {}
                        for label in connected_labels:
                            working_dictionary[label] = unlabeled_dictionary[label]
                        x_unlabeled, y_label = preprocess_data(working_dictionary, embedding_dict,\
                        mode = 'unlabeled', scaler = None)
                        predictions, uncertainty = predict_with_uncertainty(model, x_unlabeled, num_iterations=100)
                        indicies = uncertainty.argsort()[-k_num:]
                        trajectory_labels = np.take(y_label, indicies)
                    else:
                        trajectory_labels = simple_sampler_edit_distance(unlabeled_seq_dict, labeled_seq_dict, number_samples = k_num)
                trajectory_unlabeled_x = []
                
                for label in trajectory_labels:
                    trajectory_unlabeled_x.append(embedding_dict[label])
                trajectory_unlabeled_x = np.array(trajectory_unlabeled_x)

                x_train_full, y_train_full, trained_scaler = preprocess_data(labeled_dictionary, embedding_dict,\
                    mode = 'labeled')
                state = get_all_states(x_train_full, y_train_full, trajectory_unlabeled_x, model)
                temp_states = np.expand_dims(state, axis = 0)
                policy_shape = policy.input_shape[1]
                n_batches = 0
                if policy_shape != k_num:
                    assert (k_num%policy_shape == 0), 'k_num in not divisible by the policy\'s expected input shape'
                    n_batches = k_num//policy_shape
                
                if n_batches:
                    best_per_batch = []
                    #slice and roll thorough. aggregate predicted best from each batch
                    for batch_number in range(n_batches):
                        start_ind = batch_number * policy_shape
                        stop_ind = (batch_number+1) * policy_shape
                        temp_states_subset = temp_states[:, start_ind:stop_ind, :]
                        action_subset = policy.predict(temp_states_subset)
                        best_action_subset = np.argmax(action_subset)
                        best_ind = best_action_subset + start_ind
                        best_per_batch.append(best_ind)
                        #this is the index of the best, with respect to the whole pool
                    #now take all of the best
                    mega_batch = np.empty([1,policy_shape,258])
                    meta_label = []
                    mega_batch_length = policy_shape
                    row_number = 0
                    for bn, bb in enumerate(best_per_batch):
                        copies = mega_batch_length // (n_batches - bn)
                        best = temp_states.take(bb, axis = 1)
                        for c in range(copies):
                            meta_label.append(trajectory_labels[bb])
                            mega_batch[:, row_number, :] = best
                            row_number +=1
                        mega_batch_length -= copies
                        
                    all_actions = policy.predict(mega_batch)
                    action = np.argmax(all_actions)
                    selected_label = meta_label[action]

                else:
                    all_actions = policy.predict(temp_states)
                    action = np.argmax(all_actions)
                    selected_label = trajectory_labels[action]

                #adding the data...
                #6-25-202: caught a bug!
                temp_dict[selected_label] = ground_truth_dict[selected_label]
                temp_seq_dict[selected_label] = unlabeled_seq_dict[selected_label]
                del unlabeled_dictionary[selected_label]
                del unlabeled_seq_dict[selected_label]
            for selected_label in temp_dict.keys():
                labeled_dictionary[selected_label] = ground_truth_dict[selected_label]
                labeled_seq_dict[selected_label] = name_to_seq[selected_label]
                
            model.set_weights(initial_weights)
            x_train, y_train, scaler = preprocess_data(labeled_dictionary, embedding_dict,\
                mode = 'labeled')

            history = model.fit(np.array(x_train), y_train, epochs = 100, verbose = 0)
            loss = history.history['loss'][-1]
            loss_log.append(loss)
            #now predict the rest
            x_eval, y_eval = preprocess_data(unlabeled_dictionary, embedding_dict, mode = 'eval')
            predictions = model.predict(x_eval)
            log_ALIL.append([rep, t, loss_log, list(labeled_dictionary.values()), predictions, y_eval])
            print(f'Completed replicte {rep} budget {t}')
    del model
    K.clear_session()
    return log_ALIL
    
def run_ALIL_dreaming(ground_truth_dict, embedding_dict, policy_path, model_path = None, replicates = 10, initial_k = 10, budget = 100, k_num = 30, cycles = 10, number_dreams = 5, dream_budget = 10, initial_split = 5, uncertainty_sample = True):
    #TODO: start with just dreaming, then add the entropy based sampling technique in
    wake_length = budget // cycles
    policy = load_model(policy_path) #policy weights should persist, and be retrained on all actions and states at the end of each replicate
    log_ALIL = []
    states = [] #:XXX moved this
    actions = []
    
    if model_path:
        model = load_model(model_path)
    else:
        model = build_conv_network(0.001, input_dims = 1900)     
    
    initial_weights = model.get_weights()

    for rep in range(0, replicates):
        unlabeled_dictionary = copy.deepcopy(ground_truth_dict)
        labeled_dictionary = {}
        loss_log = []

        #start with a random sample #TODO Fix this, so you can start without it?
        if initial_k:
            labels = random.sample(unlabeled_dictionary.keys(), k = initial_k)
            for label in labels:
                labeled_dictionary[label] = unlabeled_dictionary[label]
                del unlabeled_dictionary[label]

            x_train, y_train, scaler = preprocess_data(labeled_dictionary, embedding_dict,\
                    mode = 'labeled')
            
            model.set_weights(initial_weights)
            history = model.fit(np.array(x_train), y_train, epochs = 100, verbose = 0)
            running_weights = model.get_weights()
            loss = history.history['loss'][-1]
            loss_log.append(loss)
        else:
            running_weights = initial_weights
            
        for t in range(0, cycles):                        
            model.set_weights(running_weights) #this will break with no initial k run longer dreams!
            #learn cycle
            for tw in range(wake_length):
                if uncertainty_sample:
                    x_unlabeled, unlabeled_ids = preprocess_data(unlabeled_dictionary, embedding_dict,\
                    mode = 'unlabeled', scaler = None)
                    predictions, uncertainty = predict_with_uncertainty(model, x_unlabeled, num_iterations=100)
                    indicies = uncertainty.argsort()[-(k_num*10):]
                    high_entropy_pool = list(np.take(unlabeled_ids, indicies))
                    trajectory_labels = random.sample(high_entropy_pool, k = k_num) #XXX: does not handle running out of data
                    
                else:
                #draw a random sample
                    if k_num > len(unlabeled_dictionary.keys()):
                        trajectory_labels = list(unlabeled_dictionary.keys())
                    else:
                        trajectory_labels = random.sample(unlabeled_dictionary.keys(), k = k_num)
                
                trajectory_unlabeled_x = []
                for label in trajectory_labels:
                    trajectory_unlabeled_x.append(embedding_dict[label])
                trajectory_unlabeled_x = np.array(trajectory_unlabeled_x)
                    #done with random sample
                
                #TODO: this block breaks if initial_k = 0... use random noise
                x_train_full, y_train_full, trained_scaler = preprocess_data(labeled_dictionary, embedding_dict,\
                    mode = 'labeled')
                state = get_all_states(x_train_full, y_train_full, trajectory_unlabeled_x, model)

                temp_states = np.expand_dims(state, axis = 0)
                all_actions = policy.predict(temp_states)
                action = np.argmax(all_actions)

                selected_label = trajectory_labels[action]
                #adding the data...
                labeled_dictionary[selected_label] = ground_truth_dict[selected_label]
                del unlabeled_dictionary[selected_label]
                
                model.set_weights(initial_weights)
                x_train, y_train, scaler = preprocess_data(labeled_dictionary, embedding_dict,\
                    mode = 'labeled')

                history = model.fit(np.array(x_train), y_train, epochs = 100, verbose = 0)
                loss = history.history['loss'][-1]
                loss_log.append(loss)
                #now predict the rest
                x_eval, y_eval = preprocess_data(unlabeled_dictionary, embedding_dict, mode = 'eval')
                predictions = model.predict(x_eval)
                log_ALIL.append([rep, (t*wake_length)+tw, loss_log, list(labeled_dictionary.values()), predictions, y_eval])
            #end learn cycle
            running_weights = model.get_weights()
            
            if (t+1) == cycles:
                continue

            #dream cycle
            for tau in range(0, number_dreams):
                dream_train_dict = copy.deepcopy(labeled_dictionary)
                validation_size = len(labeled_dictionary) // 4
                dream_unlabeled_dict = copy.deepcopy(unlabeled_dictionary)
                labels = random.sample(dream_train_dict.keys(), k = validation_size)
                validation_dictionary = {}
                for label in labels:
                    validation_dictionary[label] = dream_train_dict[label]
                    del dream_train_dict[label]

                model.set_weights(initial_weights)
                x_train, y_train, scaler = preprocess_data(dream_train_dict, embedding_dict, mode = 'labeled')
                model.fit(np.array(x_train), y_train, epochs = 100, verbose = 0)
                dream_weights = model.get_weights()

                for t in range(0, dream_budget):
                    #draw a random sample
                    if k_num > len(dream_unlabeled_dict.keys()):
                        trajectory_labels = list(dream_unlabeled_dict.keys())
                    else:
                        trajectory_labels = random.sample(dream_unlabeled_dict.keys(), k = k_num)
                    trajectory_unlabeled_x = []
                    for label in trajectory_labels:
                        trajectory_unlabeled_x.append(embedding_dict[label])
                    trajectory_unlabeled_x = np.array(trajectory_unlabeled_x)
                    #done with random sample
                
                    x_sampled = []
                    y_sampled = []
                    loss = 1e10
                    row = 0
                    best_index = 0
                    
                    x_train_full, y_train_full, trained_scaler = preprocess_data(dream_train_dict, embedding_dict,\
                        mode = 'labeled')
                    x_train_full = np.array(x_train_full)
                    y_train_full = np.array(y_train_full)
                    state = get_all_states(x_train_full, y_train_full, trajectory_unlabeled_x, model)
                    
                    #construct pseudo-ground truth dict!
                    y_traj_predicted = model.predict(trajectory_unlabeled_x)
                    
                    coin = np.random.rand(1)
                    if ( coin > 0.5 ):
                        temp_states = np.expand_dims(state, axis = 0)
                        all_actions = policy.predict(temp_states)
                        action = np.argmax(all_actions)
                    else:
                        #rollout
                        x_val, y_val, _scaler = preprocess_data(validation_dictionary, embedding_dict,\
                                mode = 'labeled', scaler = trained_scaler)
                        x_val = np.array(x_val)
                        y_val = np.array(y_val)
                        
                        for i, single_label in enumerate(trajectory_labels):
                            y_temp_raw =  { single_label : y_traj_predicted[i] }

                            x_train_temp, y_train_temp, _scaler = preprocess_data(y_temp_raw, embedding_dict,\
                                mode = 'labeled', scaler = trained_scaler)

                            model.set_weights(dream_weights)
                            history = model.fit(x_train_temp, y_train_temp,\
                                validation_data = (x_val, y_val), epochs = 1, verbose = 0)
                            val_loss = history.history['val_loss'][-1]
                            #if we improved over previous best, notate metric and index
                            if ( val_loss < loss ):
                                best_index = i
                                loss = val_loss
                        #end rollout
                        action = best_index

                    states.append(state)
                    actions.append(action)

                    selected_label = trajectory_labels[action]
                    #adding the data...
                    dream_train_dict[selected_label] = ground_truth_dict[selected_label]
                    del dream_unlabeled_dict[selected_label]

                    model.set_weights(initial_weights)
                    x_train, y_train, scaler = preprocess_data(dream_train_dict, embedding_dict,\
                        mode = 'labeled')
                        
                    #need to reselect network to take into account training on ONE data 
                    model.fit(np.array(x_train), y_train, epochs = 100, verbose = 0)
                    dream_weights = model.get_weights()
                    
                current_states = np.array(states)
                current_actions = to_categorical(np.asarray(actions), num_classes=k_num)
                ES = EarlyStopping(monitor='val_accuracy', min_delta=0.001, patience=20, verbose=0, mode='max')
                train_history = policy.fit(current_states, current_actions, verbose = 0, epochs = 1000, validation_split = 0.2, callbacks = [ES])
            #end dream cycle
        #end cycle
    #end replicate
    del model
    K.clear_session()
    return log_ALIL
   
   
#is this done? check it, i think it is not (8-15-2020)
def run_ALIL_transfer_halucinate(ground_truth_dict, embedding_dict, name_to_seq, policy_path, model_path = None, replicates = 10, initial_k = 10, budget = 10, k_num = 10, parallel_label = 8, n_seeds = 1, uncertainty_sample = False):
    policy = load_model(policy_path)
    log_ALIL = []
    if model_path:
        model = load_model(model_path)
    else:
        model = build_conv_network(0.001, input_dims = 1900)
    for rep in range(0, replicates):
        unlabeled_dictionary = copy.deepcopy(ground_truth_dict)
        unlabeled_seq_dict = copy.deepcopy(name_to_seq)
        labeled_dictionary = {}
        loss_log = []
        
        #pretend we only have this many labeled data to start:
        seeds = constrained_start(unlabeled_seq_dict, number_seeds = n_seeds, min_connections = k_num)
        labeled_seq_dict = {}
        labeled_dictionary = {}
        for seed in seeds:
            labeled_seq_dict[seed] = unlabeled_seq_dict[seed]
            labeled_dictionary[seed] = unlabeled_dictionary[seed]
            del unlabeled_seq_dict[seed]
            del unlabeled_dictionary[seed]

        for n in range(initial_k - n_seeds):
            labels = simple_sampler_edit_distance(unlabeled_seq_dict, labeled_seq_dict, number_samples = 1)
            for label in labels:
                labeled_seq_dict[label] = unlabeled_seq_dict[label]
                labeled_dictionary[label] = unlabeled_dictionary[label]
                del unlabeled_seq_dict[label]
                del unlabeled_dictionary[label]

        x_train, y_train, scaler = preprocess_data(labeled_dictionary, embedding_dict,\
                mode = 'labeled')

        history = model.fit(np.array(x_train), y_train, epochs = 100, verbose = 0)
        loss = history.history['loss'][-1]
        loss_log.append(loss)
        initial_weights = model.get_weights()
        #end added this 4-15
        
        for t in range(0, budget):
            pseudo_labels = []
            for n in range(parallel_label):
                if k_num > len(unlabeled_dictionary.keys()):
                    trajectory_labels = list(unlabeled_dictionary.keys())
                else:
                    if uncertainty_sample:
                        connected_labels = simple_sampler_edit_distance(unlabeled_seq_dict, labeled_seq_dict, number_samples = 'all')
                        working_dictionary = {}
                        for label in connected_labels:
                            working_dictionary[label] = unlabeled_dictionary[label]
                        x_unlabeled, y_label = preprocess_data(working_dictionary, embedding_dict,\
                        mode = 'unlabeled', scaler = None)
                        predictions, uncertainty = predict_with_uncertainty(model, x_unlabeled, num_iterations=100)
                        indicies = uncertainty.argsort()[-k_num:]
                        trajectory_labels = np.take(y_label, indicies)
                    else:
                        trajectory_labels = simple_sampler_edit_distance(unlabeled_seq_dict, labeled_seq_dict, number_samples = k_num)
                trajectory_unlabeled_x = []
                
                for label in trajectory_labels:
                    trajectory_unlabeled_x.append(embedding_dict[label])
                trajectory_unlabeled_x = np.array(trajectory_unlabeled_x)

                x_train_full, y_train_full, trained_scaler = preprocess_data(labeled_dictionary, embedding_dict,\
                    mode = 'labeled')
                state = get_all_states(x_train_full, y_train_full, trajectory_unlabeled_x, model)
                temp_states = np.expand_dims(state, axis = 0)
                policy_shape = policy.input_shape[1]

                all_actions = policy.predict(temp_states)
                action = np.argmax(all_actions)
                selected_label = trajectory_labels[action]

                del unlabeled_dictionary[selected_label]
                labeled_seq_dict[selected_label] = unlabeled_seq_dict[selected_label]
                del unlabeled_seq_dict[selected_label]
                
                #predict and add!
                x_unlabeled_halucinate, unlabeled_ids = preprocess_data(selected_label, embedding_dict,\
                mode = 'unlabeled')

                #construct pseudo-ground
                y_unlabeled_halucinate = model.predict(x_unlabeled_halucinate)
                labeled_dictionary[selected_label] = y_unlabeled_halucinate
                pseudo_labels.append(selected_label)

            for selected_label in pseudo_labels:
                labeled_dictionary[selected_label] = ground_truth_dict[selected_label]
                
            model.set_weights(initial_weights)
            x_train, y_train, scaler = preprocess_data(labeled_dictionary, embedding_dict,\
                mode = 'labeled')

            history = model.fit(np.array(x_train), y_train, epochs = 100, verbose = 0)
            loss = history.history['loss'][-1]
            loss_log.append(loss)
            #now predict the rest
            x_eval, y_eval = preprocess_data(unlabeled_dictionary, embedding_dict, mode = 'eval')
            predictions = model.predict(x_eval)
            log_ALIL.append([rep, t, loss_log, list(labeled_dictionary.values()), predictions, y_eval])
    del model
    K.clear_session()
    return log_ALIL
