from setuptools import setup, find_packages

setup(name='ALILPE',
      version='0.1.0',
      author='Aiden Aceves',
      author_email='aaceves@caltech.edu',
      description='Active Learning Imitation Learning for Protein Engineering',
      packages=find_packages(),
      install_requires=[],
     )
