import random
import pickle
import glob
import os
import inspect

import numpy as np

import tensorflow as tf
import argparse
from tensorflow import keras
from tensorflow.keras.models import Model, load_model, Sequential
from tensorflow.keras import backend as K
from ALILPE.Utils import init_logger, load_dataset, get_run_name
from ALILPE.Baselines import run_entropy_sampling, run_random_sampling, run_greedy_sampling
from ALILPE.ALIL import run_ALIL_transfer, run_ALIL_dreaming, run_ALIL_transfer_halucinate
from ALILPE.Utils import preprocess_data

###############################################################################################
parser = argparse.ArgumentParser(description='Transfer a Policy with ALIL')
parser.add_argument('--policy', default='/home/aiden/Documents/Mayo/ActiveLearning/ALILPE/models_and_policies/run_1586987471/RunSimulation_mako_1586987471_policy.h5', help='policy to utilize')
#might need to retrain scFv policy, and or adjust scaler
parser.add_argument('--model', default='/home/aiden/Documents/Mayo/ActiveLearning/ALILPE/models_and_policies/run_1586987471/RunSimulation_mako_1586987471_classifier.h5', help='pre-trained model for warm start')
parser.add_argument('--test_data', default='/home/aiden/Documents/Mayo/ActiveLearning/datasets/ProtaBank_pfN5Rjzn/seperate_chains/heavy/', help='data set to run tests on')
parser.add_argument('--runs', default=['ALIL_cold', 'ALIL_warm'], help='models to run, a list')
#['warm_dream', 'cold_dream', 'random_warm', 'random_cold', 'greedy_warm', 'greedy_cold', 'ALIL_cold', 'ALIL_warm', 'uncertainty_cold', 'uncertainty_warm']
parser.add_argument('--budget', default=10, help='number of data to add to model')
parser.add_argument('--replicates', default=20, help='number of replicates', type = int)
parser.add_argument('--initial_k', default=10, help='number of data to seed the model with', type=int)
parser.add_argument('--direction', default='low', help='for the greedy sampler: keep the lowest or the highest value')
parser.add_argument('--k_num', default=30, help='number of data to consider (pool size)', type = int)
parser.add_argument('--parallel_label', default=10, help='number of data to label in parallel', type = int)
parser.add_argument('--cycles', default = 20, help='Number of wake/dream cycles', type = int)
parser.add_argument('--n_dreams', default = 10, help='Number of dreams per cycle', type = int)
parser.add_argument('--dream_budget', default = 10, help='In each dream, the number of data to accept pseudo-label', type = int)
parser.add_argument('--uncertainty_sample', help='whether or not to use uncertainty sampling in the wake phase', action = 'store_true')
parser.add_argument('--n_seeds', default=1, help='number of initally labeled data to constrain edit distance', type = int)
args = parser.parse_args()

#NEED TO ADD EDIT DISTANCE!
# parse
###############################################################################################
policy = args.policy
model = args.model
test_data = args.test_data
runs = args.runs
budget = args.budget
replicates = args.replicates
initial_k = args.initial_k
k_num = args.k_num
direction = args.direction
cycles = args.cycles
n_dreams = args.n_dreams
dream_budget = args.dream_budget
uncertainty_sample = args.uncertainty_sample
parallel_label = args.parallel_label
n_seeds = args.n_seeds
###############################################################################################

physical_devices = tf.config.experimental.list_physical_devices('GPU')
assert len(physical_devices) > 0, "Not enough GPU hardware devices available"
tf.config.experimental.set_memory_growth(physical_devices[0], True)

logger = init_logger(log_file = None)
host, stamp = get_run_name()
os.mkdir('Results/run_' + stamp)
    
run_name = 'Results/run_' + stamp + '/TransferPolicy_compare_' + host + '_' + stamp 
with open(run_name + '_args.txt', 'w') as handle:
    handle.write('V2: Edit distance constrained')
    handle.write('\n\n')
    handle.write(str(args))
    handle.write('\n\n')
    handle.write(inspect.getsource(preprocess_data))
    handle.write('\n\n')
    handle.write(inspect.getsource(run_ALIL_transfer))
    handle.write('\n\n')
    handle.write(inspect.getsource(run_ALIL_dreaming))
    
ground_truth_dict, embedding_dict, name_to_seq = load_dataset(test_data)

if 'ALIL_warm' in runs:
    logger.info('Running warm-start ALIL sampling')
    log_warm_ALIL = run_ALIL_transfer(ground_truth_dict, embedding_dict, name_to_seq, policy, model_path = model,\
    parallel_label = parallel_label, replicates = replicates, initial_k = initial_k, budget = budget, k_num = k_num, n_seeds = n_seeds,\
    uncertainty_sample = uncertainty_sample)
    pickle.dump(log_warm_ALIL, open(run_name + '_warm_ALIL.pickle', 'wb'))
    
if 'ALIL_cold' in runs:
    logger.info('Running cold-start ALIL sampling')
    log_cold_ALIL = run_ALIL_transfer(ground_truth_dict, embedding_dict, name_to_seq, policy, model_path = None,\
    parallel_label = parallel_label, replicates = replicates, initial_k = initial_k, budget = budget, k_num = k_num, n_seeds = n_seeds,\
    uncertainty_sample = uncertainty_sample)
    pickle.dump(log_cold_ALIL, open(run_name + '_cold_ALIL.pickle', 'wb'))
    
if 'ALIL_warm_halucinate' in runs:
    logger.info('Running warm-start ALIL halucinate sampling')
    log_warm_ALIL = run_ALIL_transfer_halucinate(ground_truth_dict, embedding_dict, name_to_seq, policy, model_path = model,\
    parallel_label = parallel_label, replicates = replicates, initial_k = initial_k, budget = budget, k_num = k_num, n_seeds = n_seeds,\
    uncertainty_sample = uncertainty_sample)
    pickle.dump(log_warm_ALIL, open(run_name + '_warm_ALIL_halucinate.pickle', 'wb'))
    
if 'ALIL_cold_halucinate' in runs:
    logger.info('Running cold-start ALIL halucinate sampling')
    log_cold_ALIL = run_ALIL_transfer_halucinate(ground_truth_dict, embedding_dict, name_to_seq, policy, model_path = None,\
    parallel_label = parallel_label, replicates = replicates, initial_k = initial_k, budget = budget, k_num = k_num, n_seeds = n_seeds,\
    uncertainty_sample = uncertainty_sample)
    pickle.dump(log_cold_ALIL, open(run_name + '_cold_ALIL_halucinate.pickle', 'wb'))
    
if 'greedy_warm' in runs:
    logger.info('Running warm-start greedy sampling')
    log_warm_greedy = run_greedy_sampling(ground_truth_dict, embedding_dict, name_to_seq, model_path = model,\
     direction = direction, replicates = replicates, initial_k = initial_k, budget = budget, n_seeds = n_seeds)
    pickle.dump(log_warm_greedy, open(run_name + '_warm_greedy.pickle', 'wb'))

if 'greedy_cold' in runs:
    logger.info('Running cold-start greedy sampling')
    log_cold_greedy = run_greedy_sampling(ground_truth_dict, embedding_dict, name_to_seq, model_path = None, direction = direction,\
                                            replicates = replicates, initial_k = initial_k, budget = budget, n_seeds = n_seeds)
    pickle.dump(log_cold_greedy, open(run_name + '_cold_greedy.pickle', 'wb'))

    
if 'uncertainty_warm' in runs:
    logger.info('Running warm-start entropy sampling')
    log_warm_entropy = run_entropy_sampling(ground_truth_dict, embedding_dict, name_to_seq, model_path = model,\
    replicates = replicates, initial_k = initial_k, budget = budget, n_seeds = n_seeds)
    pickle.dump(log_warm_entropy, open(run_name + '_warm_entropy.pickle', 'wb'))

if 'uncertainty_cold' in runs:
    logger.info('Running cold-start entropy sampling')
    log_cold_entropy = run_entropy_sampling(ground_truth_dict, embedding_dict, name_to_seq, model_path = None, \
    replicates = replicates, initial_k = initial_k, budget = budget, n_seeds = n_seeds)
    pickle.dump(log_cold_entropy, open(run_name + '_cold_entropy.pickle', 'wb'))

if 'random_warm' in runs:
    logger.info('Running warm-start random sampling')
    log_cold_random = run_random_sampling(ground_truth_dict, embedding_dict, name_to_seq, model_path = model, \
    replicates = replicates, initial_k = initial_k, budget = budget, n_seeds = n_seeds)
    pickle.dump(log_cold_random, open(run_name + '_warm_random.pickle', 'wb'))

if 'random_cold' in runs:
    logger.info('Running cold-start random sampling')
    log_warm_random = run_random_sampling(ground_truth_dict, embedding_dict, name_to_seq, model_path = None, \
    replicates = replicates, initial_k = initial_k, budget = budget, n_seeds = n_seeds)
    pickle.dump(log_warm_random, open(run_name + '_cold_random.pickle', 'wb'))

#not constraint aware below here
if 'warm_dream' in runs:
    logger.info('Running warm-start ALIL-dream sampling')
    log_warm_dream = run_ALIL_dreaming(ground_truth_dict, embedding_dict, policy, model_path = model, replicates = replicates,\
                                            initial_k = initial_k, budget = budget, k_num = k_num, cycles = cycles,\
                                            number_dreams = n_dreams, dream_budget = dream_budget, uncertainty_sample = uncertainty_sample)
    pickle.dump(log_warm_dream, open(run_name + '_warm_dream.pickle', 'wb'))
    
    
if 'cold_dream' in runs:
    logger.info('Running cold-start ALIL-dream sampling')
    log_cold_dream = run_ALIL_dreaming(ground_truth_dict, embedding_dict, policy, model_path = None, replicates = replicates,\
                                            initial_k = initial_k, budget = budget, k_num = k_num, cycles = cycles,\
                                            number_dreams = n_dreams, dream_budget = dream_budget, uncertainty_sample = uncertainty_sample)

    pickle.dump(log_cold_dream, open(run_name + '_cold_dream.pickle', 'wb'))
