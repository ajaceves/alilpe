#!/bin/bash
input_file=$1
base_name=${input_file%.*}

sed -i 's/\,/\t/g;s/\"//g' $input_file
awk '{print ">"$1"\n"$2}' $input_file > $base_name"_VH.fasta"
awk '{print ">"$1"\n"$3}' $input_file > $base_name"_VL.fasta"
source ~/.envs/tape/bin/activate
tape-embed unirep $base_name"_VH.fasta" $base_name"_VH_full_output.npz" babbler-1900 --tokenizer unirep --full_sequence_embed
tape-embed unirep $base_name"_VL.fasta" $base_name"_VL_full_output.npz" babbler-1900 --tokenizer unirep --full_sequence_embed
deactivate
python /home/aiden/Documents/Mayo/ActiveLearning/datasets/Extract_Invariant_Positions_Two_Chains.py $input_file $base_name"_VH_full_output.npz" $base_name"_VL_full_output.npz"
