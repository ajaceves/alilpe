#!/usr/bin/env python
# coding: utf-8

# In[11]:


#input file 1 should have name, sequence, Y-value in tab-delimited format
#input file 2 should be VH embeddings (full length)
#input file 3 should be VL embeddings (full length)
#embedding command:
#for files in ~/Documents/Mayo/ActiveLearning/datasets/ProtaBank_PpQa5ft44/*.fasta; do name=${files%%.*}; tape-embed unirep $files $name".npz" babbler-1900 --tokenizer unirep --full_sequence_embed ; done

import csv
import os
import pickle
import glob
import sys
import numpy as np

input_path = sys.argv[1]
VH_embedding_path = sys.argv[2]
VL_embedding_path = sys.argv[3]
with open(input_path, 'r') as csvfile:
    reader = csv.reader(csvfile, delimiter='\t')
    data = list(reader)

#some antibodies have duplicate Kd measurements in the set. Average them:
VH_data = []
VL_data = []
for i, d in enumerate(data):
    name, VH, VL, Y = d
    VH_data.append([c for c in VH])
    VL_data.append([c for c in VL])

#splits the antibody sequences up into a list
VH_data = np.array(VH_data)
VL_data = np.array(VL_data)

VH_variable_columns = ~np.all(VH_data == VH_data[0,:], axis = 0)
VL_variable_columns = ~np.all(VL_data == VL_data[0,:], axis = 0)

#the state space is all of the sequences that we have data for
VH_variable_columns = [i+1 for i, val in enumerate(VH_variable_columns) if val]
VL_variable_columns = [i+1 for i, val in enumerate(VL_variable_columns) if val]

all_extracted = {}
selected_mean = {}
VH_arrays = np.load(VH_embedding_path, allow_pickle=True)
VL_arrays = np.load(VL_embedding_path, allow_pickle=True)
VH_keys = list(VH_arrays.keys())
for key in VH_keys:
    VH_full = VH_arrays[key][()]['seq']
    VL_full = VL_arrays[key][()]['seq']
    VH_extracted = np.take(VH_full, VH_variable_columns, axis = 0)
    VL_extracted = np.take(VL_full, VL_variable_columns, axis = 0)
    merged = np.concatenate([VH_extracted, VL_extracted], axis = 0)
    sel_mean = np.mean(merged, axis = 0)
    selected_mean[key] = sel_mean


output_path = os.path.dirname(VH_embedding_path)
pickle.dump(selected_mean, open(output_path + '/extracted_positions_avg.pickle', 'wb'))

print('Embeddings extracted successfully!')
