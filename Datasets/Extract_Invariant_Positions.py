#!/usr/bin/env python
# coding: utf-8

# In[11]:


#input file 1 should have name, sequence, Y-value in tab-delimited format
#input file 2 should be embeddings (full length)
#embedding command:
#for files in ~/Documents/Mayo/ActiveLearning/datasets/ProtaBank_PpQa5ft44/*.fasta; do name=${files%%.*}; tape-embed unirep $files $name".npz" babbler-1900 --tokenizer unirep --full_sequence_embed ; done


# In[34]:


import csv
import os
import pickle
import glob
import sys
import numpy as np


# In[30]:


input_path = sys.argv[1]
embedding_path = sys.argv[2]
with open(input_path, 'r') as csvfile:
    reader = csv.reader(csvfile, delimiter='\t')
    data = list(reader)

#some antibodies have duplicate Kd measurements in the set. Average them:
averaged_data = {}
names_to_keep = {}
for i, d in enumerate(data):
    name, seq, Y = d
    if seq in averaged_data.keys():
        averaged_data[seq] += [Y]
    else:
        names_to_keep[seq] = name
        averaged_data[seq] = [Y]

ground_truth = {}
for seq, label in averaged_data.items():
    value = np.array(label, dtype=float).mean()
    name = names_to_keep[seq]
    ground_truth[name] = value


# In[31]:


merged_chains = []
for seq, label in averaged_data.items():
    merged_chains.append([c for c in seq])

#splits the antibody sequences up into a list
merged_chains = np.array(merged_chains)

#we only care about the positions that are not invariant across the entire set:
variable_columns = ~np.all(merged_chains == merged_chains[0,:], axis = 0)

#the state space is all of the sequences that we have data for
state_space = merged_chains[:,variable_columns]


# In[32]:


#how are embeddings length 440? all sequences are 438. Start and stop token?
variable = [i+1 for i, val in enumerate(variable_columns) if val]
#zero indexed, including token
#try using pool of whole thing, full vector only for variable positions, and average of variable positions


# In[33]:


all_pooled = {}
all_extracted = {}
all_mean = {}
selected_mean = {}
arrays = np.load(embedding_path, allow_pickle=True)
keys = list(arrays.keys())
for key in keys:
    if key in names_to_keep.values():
        full = arrays[key][()]['seq']
        extracted = np.take(full, variable, axis = 0)
        all_extracted[key] = extracted
        mean = np.mean(full, axis = 0)
        all_mean[key] = mean
        sel_mean = np.mean(extracted, axis = 0)
        selected_mean[key] = sel_mean
        pooled = arrays[key][()]['pooled']
        all_pooled[key] = pooled


# In[35]:


output_path = os.path.dirname(embedding_path)


# In[36]:


pickle.dump(all_extracted, open(output_path + '/extracted_positions_full_length.pickle', 'wb'))
pickle.dump(all_mean, open(output_path + '/all_positions_avg.pickle', 'wb'))
pickle.dump(selected_mean, open(output_path + '/extracted_positions_avg.pickle', 'wb'))
pickle.dump(all_pooled, open(output_path + '/full_length_pooled.pickle', 'wb'))


# In[37]:


print('Embeddings extracted successfully!')


# In[ ]:




