#!/bin/bash
input_file=$1
base_name=${input_file%.*}

sed -i 's/\,/\t/g;s/\"//g' $input_file
awk '{print ">"$1"\n"$2}' $input_file > $base_name.fasta
source ~/.envs/tape/bin/activate
tape-embed unirep $base_name.fasta $base_name"_full_output.npz" babbler-1900 --tokenizer unirep --full_sequence_embed
deactivate
python /home/aiden/Documents/Mayo/ActiveLearning/datasets/Extract_Invariant_Positions.py $input_file $base_name"_full_output.npz"
