import numpy as np
import tensorflow as tf

from tensorflow.keras import backend as K
from tensorflow.keras.models import Model, load_model, Sequential
from tensorflow.keras.losses import categorical_crossentropy
from tensorflow.keras import Input
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.regularizers import l2
from tensorflow.keras.layers import Dense, Dropout, Flatten, multiply, TimeDistributed, Activation, Input, Reshape, Conv1D

def build_conv_network(lr, input_dims = 1900):
    inputs = Input(shape=(input_dims,))
    attention_probs = Dense(input_dims, activation='linear', name='attention_probs')(inputs)
    dense = multiply([inputs, attention_probs])
    dense = Reshape(target_shape=(1900,1,))(dense)
    conv = Conv1D(128, 3, activation = 'relu', data_format='channels_last')(dense)
    dense = Flatten()(conv)
    dense = Dense(128, activation = 'relu')(dense)
    dense = Dense(128, activation = 'relu', name = 'intermediate')(dense)
    dense = Dropout(0.5)(dense)
    output = Dense(1, activation='linear')(dense)

    complete_network = Model(inputs=[inputs], outputs=[output])
    complete_network.compile(optimizer=Adam(lr), loss = 'mse')
    return complete_network
    
def build_conv_network_entropy(lr, input_dims = 1900):
    inputs = Input(shape=(input_dims,))
    attention_probs = Dense(input_dims, activation='linear', name='attention_probs')(inputs)
    dense = multiply([inputs, attention_probs])
    dense = Reshape(target_shape=(1900,1,))(dense)
    conv = Conv1D(128, 3, activation = 'relu', data_format='channels_last')(dense)
    dense = Flatten()(conv)
    dense = Dense(128, activation = 'relu')(dense)
    dense = Dense(128, activation = 'relu', name = 'intermediate')(dense)
    dense = Dropout(0.5)(dense, training = True)
    output = Dense(1, activation='linear')(dense)

    complete_network = Model(inputs=[inputs], outputs=[output])
    complete_network.compile(optimizer=Adam(lr), loss = 'mse')
    return complete_network
    
#the state is taken as: sum of all the embeddings in train, the ratio of classes
#the sum of embeddings in the unlabeled, and the predicted labels for all unlabeled
def get_policy(k, state_dim, learning_rate = 1e-3):
    #TODO: Why time distributed, and not allowing flexible batch sizes?
    policy = Sequential()
    policy.add(TimeDistributed(Dense(1),\
                input_shape=(k, state_dim)))
    policy.add(Reshape((k,), input_shape=(k,1)))
    policy.add(Activation('softmax'))
    optimizer = Adam(lr=learning_rate)
    policy.compile(loss='categorical_crossentropy',
              optimizer=optimizer, metrics=['mse', 'accuracy'])
    return policy
    
def get_policy_submodular(k, state_dim):
    #TODO: Why time distributed, and not allowing flexible batch sizes?
    policy = Sequential()
    policy.add(TimeDistributed(Dense(1),\
                input_shape=(k, state_dim)))
    policy.add(Reshape((k,), input_shape=(k,1)))
    policy.add(Activation('softmax'))
    return policy
    
class SubmodularModel(Model):
    def __init__(self, policy_model, lam):
        super(SubmodularModel, self).__init__()
        self.policy_model = policy_model
        self.lam = lam
        
    def compile(self, optimizer):
        super(SubmodularModel, self).compile()
        self.optimizer = optimizer

    def train_step(self, data):
        [x, x_super], y = data
        with tf.GradientTape() as tape:
            #resume here
            y_pred = self.policy_model(x, training = True)
            y_super = self.policy_model(x_super, training = True)
            #there are two models, one for superhistory, one for normal
            loss_standard = categorical_crossentropy(y, y_pred)
            regularizer = self.lam * K.sum( K.sigmoid( y_super - y_pred))
            loss = loss_standard + regularizer

        trainable_vars = self.trainable_variables
        gradients = tape.gradient(loss, trainable_vars)
        self.optimizer.apply_gradients(zip(gradients, trainable_vars))
        self.compiled_metrics.update_state(y, y_pred)
        return {m.name: m.result() for m in self.policy_model.metrics}
        
    def eval(self, data):
        [x, x_super], y = data
        y_pred = self.policy_model(x, training = True)

        self.compiled_metrics.update_state(y, y_pred)
        return {m.name: m.result() for m in self.policy_model.metrics}

    def call(self, data, training = False):
        print(self.policy_model.input)
        return self.policy_model(data)
        
    def test_step(self, data):
        [x, x_super], y = data
        y_pred = self(x, training=False)
        self.compiled_loss(y, y_pred)
        # Update the metrics.
        self.compiled_metrics.update_state(y, y_pred)
        # Note that it will include the loss (tracked in self.metrics).
        return {m.name: m.result() for m in self.metrics}

def build_simple_network(lr, do, input_dims = 1900):
    inputs = Input(shape=(input_dims,))
    attention_probs = Dense(input_dims, activation='softmax', name='attention_probs')(inputs)
    attention_mul = multiply([inputs, attention_probs])
    
    #this would be the layer they use as the representation for the policy net:
    dense = Dense(512, activation='relu', name = 'intermediate')(attention_mul)
    output = Dense(1, activation='linear')(dense)

    complete_network = Model(inputs=[inputs], outputs=[output])
    complete_network.compile(optimizer=Adam(lr), loss = 'mse')
    return complete_network
