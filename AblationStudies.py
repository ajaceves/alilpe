import random
import pickle
import glob
import os
import inspect

import numpy as np

import tensorflow as tf
import argparse
from tensorflow import keras
from tensorflow.keras.models import Model, load_model, Sequential
from tensorflow.keras import backend as K
from ALILPE.Utils import init_logger, load_dataset, get_run_name
from ALILPE.ALIL_ablation import run_ALIL_transfer
from ALILPE.Utils import preprocess_data

###############################################################################################
parser = argparse.ArgumentParser(description='Transfer a Policy with ALIL')
parser.add_argument('--policy', default='/home/aiden/Documents/Mayo/ActiveLearning/ALILPE/models_and_policies/run_1586987471/RunSimulation_mako_1586987471_policy.h5', help='policy to utilize')
#might need to retrain scFv policy, and or adjust scaler
parser.add_argument('--model', default='/home/aiden/Documents/Mayo/ActiveLearning/ALILPE/models_and_policies/run_1586987471/RunSimulation_mako_1586987471_classifier.h5', help='pre-trained model for warm start')
parser.add_argument('--test_data', default='/home/aiden/Documents/Mayo/ActiveLearning/datasets/ProtaBank_pfN5Rjzn/seperate_chains/heavy/', help='data set to run tests on')
parser.add_argument('--runs', default=['ALIL_cold', 'ALIL_warm'], help='models to run, a list')
parser.add_argument('--budget', default=100, help='number of data to add to model')
parser.add_argument('--replicates', default=20, help='number of replicates', type = int)
parser.add_argument('--initial_k', default=10, help='number of data to seed the model with', type=int)
parser.add_argument('--direction', default='low', help='for the greedy sampler: keep the lowest or the highest value')
parser.add_argument('--k_num', default=30, help='number of data to consider (pool size)', type = int)
parser.add_argument('--parallel_label', default=1, help='number of data to label in parallel', type = int)
parser.add_argument('--ablation', default=0, help='part of policy input to ablate', type = int)
args = parser.parse_args()

# parse
###############################################################################################
policy = args.policy
model = args.model
test_data = args.test_data
runs = args.runs
budget = args.budget
replicates = args.replicates
initial_k = args.initial_k
k_num = args.k_num
direction = args.direction
parallel_label = args.parallel_label
ablation = args.ablation

###############################################################################################

physical_devices = tf.config.experimental.list_physical_devices('GPU')
assert len(physical_devices) > 0, "Not enough GPU hardware devices available"
tf.config.experimental.set_memory_growth(physical_devices[0], True)

logger = init_logger(log_file = None)
host, stamp = get_run_name()
os.mkdir('Results/run_' + stamp)
    
run_name = 'Results/run_' + stamp + '/TransferPolicy_compare_' + host + '_' + stamp 
with open(run_name + '_args.txt', 'w') as handle:
    handle.write(str(args))
    handle.write('\n\n')
    handle.write(inspect.getsource(preprocess_data))
    handle.write('\n\n')
    handle.write(inspect.getsource(run_ALIL_transfer))
    
ground_truth_dict, embedding_dict = load_dataset(test_data)

if 'ALIL_warm' in runs:
    logger.info('Running warm-start ALIL sampling')
    log_warm_ALIL = run_ALIL_transfer(ground_truth_dict, embedding_dict, policy, model_path = model, parallel_label = parallel_label,\
                                            replicates = replicates, initial_k = initial_k, budget = budget, k_num = k_num, ablate = ablation)
    pickle.dump(log_warm_ALIL, open(run_name + '_warm_ALIL.pickle', 'wb'))
    
if 'ALIL_cold' in runs:
    logger.info('Running cold-start ALIL sampling')
    log_cold_ALIL = run_ALIL_transfer(ground_truth_dict, embedding_dict, policy, model_path = None, parallel_label = parallel_label,\
                                            replicates = replicates, initial_k = initial_k, budget = budget, k_num = k_num, ablate = ablation)
    pickle.dump(log_cold_ALIL, open(run_name + '_cold_ALIL.pickle', 'wb'))
