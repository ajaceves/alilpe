import random
import pickle
import glob
import os
import copy

import numpy as np

import tensorflow as tf
import argparse
from tensorflow import keras
from tensorflow.keras.models import Model, load_model, Sequential
from tensorflow.keras import backend as K
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.callbacks import EarlyStopping
from Utils import init_logger, load_dataset, get_run_name
from ALILPE.Models import build_simple_network, get_policy
from ALILPE.Utils import init_logger, load_dataset, get_run_name, preprocess_data, get_a_state, get_all_states

###############################################################################################
parser = argparse.ArgumentParser(description='Train a Policy with ALIL')
parser.add_argument('--train_data', default='/home/aiden/Documents/Mayo/ActiveLearning/datasets/ProtaBank_3xESLyS9/', help='data set to run tests on')
parser.add_argument('--budget', default=100, help='number of data to add to model')
parser.add_argument('--episodes', default=100, help='number of episodes (replicates)')
parser.add_argument('--initial_k', default=100, help='number of data to seed the model with')
parser.add_argument('--initial_split', default=30, help='number of data to use for evaluating progress')
parser.add_argument('--k_num', default=30, help='number of data to consider (pool size)')
args = parser.parse_args()

# parse
###############################################################################################
train_data = args.train_data
budget = args.budget
episodes = args.episodes
initial_k = args.initial_k
initial_split = args.initial_split
k_num = args.k_num
###############################################################################################

physical_devices = tf.config.experimental.list_physical_devices('GPU')
assert len(physical_devices) > 0, "Not enough GPU hardware devices available"
tf.config.experimental.set_memory_growth(physical_devices[0], True)

logger = init_logger(log_file = None)
host, stamp = get_run_name()
os.mkdir('models_and_policies/run_' + stamp)
    
run_name = 'models_and_policies/run_' + stamp + '/RunSimulation_' + host + '_' + stamp 
with open(run_name + '_args.txt', 'w') as handle:
    handle.write(str(args))
    
ground_truth_dict, embedding_dict = load_dataset(train_data)

physical_devices = tf.config.experimental.list_physical_devices('GPU')
assert len(physical_devices) > 0, "Not enough GPU hardware devices available"
tf.config.experimental.set_memory_growth(physical_devices[0], True)

#main code
###############################################################################################
policy_name = run_name + '_policy.h5'
classifier_name = run_name + '_classifier.h5'
policy = get_policy(k_num, 1026)
policy.save(policy_name)
states = []
actions = []

logger.info('Begin training AL policy:')
for tau in range(0, episodes):
    logger.info(f' * Start episode {tau}')
    logger.info("Seed experiment with initial labeling:")
    unlabeled_dictionary = copy.deepcopy(ground_truth_dict)
    labels = random.sample(unlabeled_dictionary.keys(), k = initial_k)
    labeled_dictionary = {}
    for label in labels:
        labeled_dictionary[label] = unlabeled_dictionary[label]
        del unlabeled_dictionary[label]

    logger.info("Split labeled data into train and validation:")
    labels = random.sample(labeled_dictionary.keys(), k = initial_split)
    validation_dictionary = {}
    for label in labels:
        validation_dictionary[label] = labeled_dictionary[label]
        del labeled_dictionary[label]
    
    logger.info(f'[Episode {tau}] Load Policy from path {policy_name}')
    policy = load_model(policy_name)

    model = build_simple_network(0.0005, 0.1, input_dims = 1900)
    initial_weights = model.get_weights()

    x_train, y_train, scaler = preprocess_data(labeled_dictionary, embedding_dict, mode = 'labeled')

    model.fit(np.array(x_train), y_train, epochs = 10, verbose = 0)

    current_weights = model.get_weights()
    model.save(classifier_name)

    #toss a coin
    coin = np.random.rand(1)
    if tau == 0:
        coin = 0
    for t in range(0, budget):
        logger.info(f'Episode:{(tau+1)} Budget:{(t+1)}')
        x_sampled = []
        y_sampled = []
        loss = 1e10
        row = 0
        best_index = 0

        trajectory_labels = random.sample(unlabeled_dictionary.keys(), k = k_num)
        trajectory_unlabeled_x = []
        for label in trajectory_labels:
            trajectory_unlabeled_x.append(embedding_dict[label])
        trajectory_unlabeled_x = np.array(trajectory_unlabeled_x)
        x_train_full, y_train_full, trained_scaler = preprocess_data(labeled_dictionary, embedding_dict,\
            mode = 'labeled')
        x_train_full = np.array(x_train_full)
        y_train_full = np.array(y_train_full)
        state = get_all_states(x_train_full, y_train_full, trajectory_unlabeled_x, model)#before or after trajectory?
        
        if ( coin > 0.5 ):
            logger.info(f'* Coin returned {coin}, using POLICY')
            #no need to do rollout!
            temp_states = np.expand_dims(state, axis = 0)
            all_actions = policy.predict(temp_states)
            action = np.argmax(all_actions)
        else:
            logger.info(f'* Coin returned {coin}, using EXPERT')
            #rollout        
            for i, single_label in enumerate(trajectory_labels):
                logger.debug(f'Episode:{(tau+1)} Budget:{(t+1)} Pool Sample {i+1}')
                temp_train_dict = copy.deepcopy(labeled_dictionary)
                temp_train_dict[single_label] = ground_truth_dict[single_label]
               
                y_temp_raw =  { single_label : unlabeled_dictionary[single_label] }

                x_train_temp, y_train_temp, _scaler = preprocess_data(y_temp_raw, embedding_dict,\
                    mode = 'labeled', scaler = trained_scaler)

                x_val, y_val, _scaler = preprocess_data(validation_dictionary, embedding_dict,\
                    mode = 'labeled', scaler = trained_scaler)

                model.set_weights(current_weights)
                x_val = np.array(x_val)
                y_val = np.array(y_val)
                x_train_temp = np.array(x_train_temp)
                y_train_temp = np.array(y_train_temp)

                history = model.fit(x_train_temp, y_train_temp,\
                    validation_data = (x_val, y_val), epochs = 10, verbose = 0)
                val_loss = history.history['val_loss'][-1] #check this slice logic
                #if we improved over previous best, notate metric and index
                if ( val_loss < loss ):
                    best_index = i
                    loss = val_loss
            #end rollout
            action = best_index
        states.append(state)
        actions.append(action)
        
        selected_label = trajectory_labels[action]
        #adding the data...
        labeled_dictionary[selected_label] = ground_truth_dict[selected_label]
        del unlabeled_dictionary[selected_label]
        
        model.set_weights(initial_weights)
        x_train, y_train, scaler = preprocess_data(labeled_dictionary, embedding_dict,\
            mode = 'labeled')
        model.fit(np.array(x_train), y_train, epochs = 10, verbose = 0)
        current_weights = model.get_weights()
        model.save(classifier_name)
        
    logger.info(f'* End of episode, training policy network using replay memory')
    #we could save all states and actions, and train policy network offline, however policy network evolves in path dependent manner, and
    #actions and states influence policy we arrive at
    #better to train while allowing mixing between episodes
    current_states = np.array(states)
    current_actions = to_categorical(np.asarray(actions), num_classes=k_num)
    ES = EarlyStopping(monitor='val_accuracy', min_delta=0.001, patience=20, verbose=0, mode='max')
    train_history = policy.fit(current_states, current_actions, verbose = 0, epochs = 1000, validation_split = 0.2, callbacks = [ES])
    logger.info(f'Episode {tau+1} Training policy loss = {train_history.history["loss"][-1]}')
        
    logger.info(f'* End episode {tau}. Saving policy to {policy_name}')
    policy.save(policy_name)
    K.clear_session()
    del model
pickle.dump(states, open(run_name + '_states.pickle', 'wb'))
pickle.dump(actions, open(run_name + '_actions.pickle', 'wb'))

#we weren't training the policy network for long enough!!??
#redo the embeddings, only one chain actually has mutations on it!
#dream wake stuff is pretty simple, we cold start with pre-trained policy and model, 
#WAKE: use the policy to predict best, annotate with oracle, retrain model ONLY DIFFERENCE IS THE STATE SPACE INCLUDES THE STUDENT LEARNER (e.g. the model)
#DREAM: Collect trajectories with current policy and roll-out (mix), retrain policy. when doing roll-out, use PREDICTED labels of D-pool
#definately need to allow "dreaming" to adapt attention layer
#why is it embedding 41x1900 as the shape of the TRAINING space?
