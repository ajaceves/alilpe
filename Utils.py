import logging
import glob
import csv
import time
import socket
import pickle
import numpy as np
from scipy.spatial.distance import hamming
from sklearn.preprocessing import MinMaxScaler
import tensorflow as tf

def init_logger(log_file = None):
    log_format = logging.Formatter("[%(asctime)s %(levelname)s] %(message)s")
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    console_handler = logging.StreamHandler()
    console_handler.setFormatter(log_format)
    logger.handlers = [console_handler]

    if log_file and log_file != '':
        file_handler = logging.FileHandler(log_file)
        file_handler.setFormatter(log_format)
        logger.addHandler(file_handler)

    return logger
    
def calculate_hamming(string_a, string_b):
    assert len(string_a) == len(string_b), "Strings are of varied length, exiting"
    return hamming(list(string_a),list(string_b)) * len(string_a)
    
def simple_sampler_edit_distance(unlabeled_sequence_dict, labeled_sequence_dict, number_samples = 10):
    '''Simple sampler that is only constrained by edit distance'''
    keys = list(unlabeled_sequence_dict.keys())
    np.random.shuffle(keys)
    labels = []
    debug = []
    ki = 0
    if number_samples == 'all':
        for sample in keys:
            for label in list(labeled_sequence_dict.keys()):
                hamm = calculate_hamming(labeled_sequence_dict[label], unlabeled_sequence_dict[sample])
                if hamm == 1.0:
                    labels.append(sample)
                    break
        return labels
    else:
        while len(labels) < number_samples:
            sample = keys[ki]
            for label in list(labeled_sequence_dict.keys()):
                hamm = calculate_hamming(labeled_sequence_dict[label], unlabeled_sequence_dict[sample])
                if hamm == 1.0:
                    labels.append(sample)
                    break
            ki += 1
    return labels
    
def constrained_start(unlabeled_sequence_dict, number_seeds = 1, min_connections = 30):
    '''constrained start only allows seed with at least min_connections that are edit distance away'''
    keys = list(unlabeled_sequence_dict.keys())
    np.random.shuffle(keys)
    seeds = []
    debug = []
    ki = 0
    while ki+1 <= len(keys):
        sample = keys[ki]
        connections = -1 #we did not remove self from list, so start at -1
        for label in keys:
            hamm = calculate_hamming(unlabeled_sequence_dict[label], unlabeled_sequence_dict[sample])
            if hamm == 1.0:
                connections += 1
            if connections >= min_connections:
                seeds.append(sample)
                break
            debug.append(connections)
        ki += 1
        if len(seeds) >= number_seeds:
            return seeds
    print(f"Best connection: {np.max(debug)}")
    raise ValueError('Unable to find sufficient starting seed(s)')
    
def load_dataset(folder_path):
    '''
    Assumes standard naming convention as demonstrated by dataprep bash script
    '''
    csv_list = glob.glob(folder_path + '/*.csv')
    assert len(csv_list) == 1, 'MORE THAN ONE CSV FILE FOUND. EXITING'
    with open(csv_list[0], 'r') as csv_handle:
        reader = csv.reader(csv_handle, delimiter = '\t')
        data = list(reader)

    #some data have duplicate Kd measurements in the set. Average them:
    averaged_data = {}
    names_to_keep = {}
    for i, d in enumerate(data):
        name, seq, Y = d
        if seq in averaged_data.keys():
            averaged_data[seq] += [Y]
        else:
            names_to_keep[seq] = name
            averaged_data[seq] = [Y]

    ground_truth_dict = {}
    seq_dict = {}
    
    for seq, label in averaged_data.items():
        value = np.array(label, dtype=float).mean()
        name = names_to_keep[seq]
        ground_truth_dict[name] = value
        seq_dict[name] = seq
        
    #load embeddings (pre-calculated, and pre-extracted to exclude invariant positions
    embedding_dict = pickle.load(open(folder_path + 'extracted_positions_avg.pickle', 'rb'))
    return ground_truth_dict, embedding_dict, seq_dict
    
def preprocess_data(data_labels, state_space, mode='labeled', scaler = None, force_zero = False):
    '''
    mode can be labeled, unlabeled, or eval
    
    THIS VERSION DOES NOT INVERT LABELS, OR LOG TRANSFORM
    '''
    if mode == 'labeled':
        #this is the line killing us. remove 5-19-2020
        #ids = [variant for variant, label in data_labels.items() if label]
        
        ids = [variant for variant, label in data_labels.items()]

        x = np.array([state_space[v] for v in ids])
        y = np.array([data_labels[v] for v in ids]).reshape(-1,1)
        if force_zero:
            y = np.concatenate([y, [[0]]])
        if not scaler:
            scaler = MinMaxScaler()
            y = scaler.fit_transform(y)
        else:
            y = scaler.transform(y)
        
        if force_zero:
            y = y[:-1]
        return x, y, scaler
    
    elif mode == 'unlabeled':
        if isinstance(data_labels, dict):
            unlabeled_ids = [variant for variant, label in data_labels.items() if label]
        elif isinstance(data_labels, list):
            unlabeled_ids = data_labels
        else:
            unlabeled_ids = [data_labels]
        x_unlabeled = np.array([state_space[v] for v in unlabeled_ids])
        return x_unlabeled, unlabeled_ids
    
    elif mode == 'eval':
        test_ids = [variant for variant, label in data_labels.items()]
        x_test = np.array([state_space[v] for v in test_ids])
        y_test = np.array([data_labels[v] for v in test_ids])
        
        return x_test, y_test
    
    else:
        raise ValueError('Invalid mode provided')
        
def get_state(x_trn, y_trn, x_new, model):
    #bag of embeddings from training set
    embeddings = get_intermediate_layer(model, x_trn)
    train_embedding = sum(embeddings)
    #summary of labels in training set
    mean_y_train = np.mean(y_trn).reshape(1,)
    #expand the dimension
    x_new = np.expand_dims(x_new, axis=0)
    #candidate embedding
    new_embedding = get_intermediate_layer(model, x_new)
    pred_embedding = sum(new_embedding)
    #expected prediction
    y_predicted = model.predict(x_new)
    prediction = sum(y_predicted)
    #concatenate all 4 arrays into a single array
    state = np.concatenate([train_embedding, mean_y_train, pred_embedding, prediction])
    return state

def get_a_state(x_trn, y_trn, x_unlabeled_list, model):
    samples = []
    #super inefficent
    for point in x_unlabeled_list:
        s = get_state( x_trn, y_trn, point, model)
        samples.append(s)
    return samples

def get_all_states(x_trn, y_trn, x_unlabeled_list, model):
    #bag of embeddings from training set
    embeddings = get_intermediate_layer(model, x_trn)
    train_embedding = sum(embeddings)
    #summary of labels in training set
    mean_y_train = np.mean(y_trn).reshape(1,)
    #expand the dimension
    samples = []
    for x_new in x_unlabeled_list:
        x_new = np.expand_dims(x_new, axis=0)
        #candidate embedding
        new_embedding = get_intermediate_layer(model, x_new)
        pred_embedding = sum(new_embedding)
        #expected prediction
        y_predicted = model.predict(x_new)
        prediction = sum(y_predicted)
        #concatenate all 4 arrays into a single array
        state = np.concatenate([train_embedding, mean_y_train, pred_embedding, prediction])
        samples.append(state)
    return samples

#list of embeddings, list of labeled values, list ofembeddings, model
def get_all_states_w_super(x_trn, y_trn, x_unlabeled_list, x_super, model):
    #bag of embeddings from training set
    embeddings = get_intermediate_layer(model, x_trn)
    train_embedding = sum(embeddings)
    #summary of labels in training set
    mean_y_train = np.mean(y_trn).reshape(1,)

    x_super = np.concatenate((x_trn, x_super))
    embeddings_super = get_intermediate_layer(model, x_super)
    embeddings_super = sum(embeddings_super)

    states = []
    superstates = []
    for x_new in x_unlabeled_list:
        x_new = np.expand_dims(x_new, axis=0)
        #candidate embedding
        new_embedding = get_intermediate_layer(model, x_new)
        pred_embedding = sum(new_embedding)
        #expected prediction
        y_predicted = model.predict(x_new)
        prediction = sum(y_predicted)
        #concatenate all 4 arrays into a single array
        state = np.concatenate([train_embedding, mean_y_train, pred_embedding, prediction])
        states.append(state)
        superstate = np.concatenate([train_embedding, mean_y_train, pred_embedding, prediction])
        superstates.append(np.concatenate([embeddings_super, mean_y_train, pred_embedding, prediction]))
    return states, superstates

def get_all_states_ablate(x_trn, y_trn, x_unlabeled_list, model, ablate = 0):
    #bag of embeddings from training set
    embeddings = get_intermediate_layer(model, x_trn)
    train_embedding = sum(embeddings)
    #summary of labels in training set
    mean_y_train = np.mean(y_trn).reshape(1,)
    #expand the dimension
    samples = []
    for x_new in x_unlabeled_list:
        x_new = np.expand_dims(x_new, axis=0)
        #candidate embedding
        new_embedding = get_intermediate_layer(model, x_new)
        pred_embedding = sum(new_embedding)
        #expected prediction
        y_predicted = model.predict(x_new)
        prediction = sum(y_predicted)
        #concatenate all 4 arrays into a single array
        if ablate == 1:
            train_embedding = np.random.uniform(-1, -1, size = train_embedding.shape)
        elif ablate == 2:
            mean_y_train = np.random.uniform(-1, -1, size = mean_y_train.shape)
        elif ablate == 3:
            pred_embedding = np.random.uniform(-1, -1, size = pred_embedding.shape)
        elif ablate == 4:
            prediction = np.random.uniform(-1, -1, size = prediction.shape)
            
        state = np.concatenate([train_embedding, mean_y_train, pred_embedding, prediction])
        samples.append(state)
    return samples

def get_intermediate_layer(model, x_batch):
    get_activations = tf.keras.Model(model.inputs,model.get_layer('intermediate').output)
    #TODO: print(x_batch.shape) *** possible bug, shape doesnt seem right...
    return get_activations([x_batch, 0])
    
def predict_with_uncertainty(model, X, num_iterations=1):
    last_layer = model.layers[-1]
    results = np.zeros((len(X), num_iterations, 1), dtype="float")
    for i in range(num_iterations):
        results[:,i,:] = model.predict(X)
    predictions = results.mean(axis=(1,2))
    uncertainty = results.std(axis=(1,2))
    return predictions, uncertainty
    
def get_run_name():
    host = socket.gethostname()
    stamp = str(int(time.time()))
    return host, stamp
    
def eval_top(y_true, y_pred, n_take, take_large = False):
    indicies = y_pred.flatten().argsort()
    if take_large:
        indicies = indicies[n_take:]
        pred_top = y_true.take(indicies)
        cutoff_ind = y_true.flatten().argsort()[-n_take]
        top_cut = y_true[cutoff_ind]
        
        recover_top = sum(pred_top >= top_cut)
    else:
        indicies = indicies[:n_take]
        pred_top = y_true.take(indicies)
        cutoff_ind = y_true.flatten().argsort()[n_take]
        top_cut = y_true[cutoff_ind]
        recover_top = sum(pred_top <= top_cut)
        
    return pred_top, np.median(pred_top), recover_top / n_take, top_cut

