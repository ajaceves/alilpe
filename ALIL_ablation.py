import copy
import random
import numpy as np
from tensorflow.keras import backend as K
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.models import load_model
from tensorflow.keras.callbacks import EarlyStopping

from Models import build_conv_network, get_policy, build_simple_network
from Utils import preprocess_data, get_all_states_ablate, predict_with_uncertainty

def run_ALIL_transfer(ground_truth_dict, embedding_dict, policy_path, model_path = None, replicates = 10, initial_k = 10, budget = 10, k_num = 10, parallel_label = 1, ablate = 0):
    policy = load_model(policy_path)
    log_ALIL = []
    if model_path:
        model = load_model(model_path)
    else:
        model = build_conv_network(0.001, input_dims = 1900)
    for rep in range(0, replicates):
        unlabeled_dictionary = copy.deepcopy(ground_truth_dict)
        labeled_dictionary = {}
        loss_log = []
        
        #start with a random sample    
        if initial_k:
            labels = random.sample(unlabeled_dictionary.keys(), k = initial_k)
            for label in labels:
                labeled_dictionary[label] = unlabeled_dictionary[label]
                del unlabeled_dictionary[label]

        #added this 4-15
        x_train, y_train, scaler = preprocess_data(labeled_dictionary, embedding_dict,\
                mode = 'labeled')

        history = model.fit(np.array(x_train), y_train, epochs = 100, verbose = 0)
        loss = history.history['loss'][-1]
        loss_log.append(loss)
        initial_weights = model.get_weights()
        #end added this 4-15
        
        for t in range(0, budget):
            for n in range(parallel_label):
                if k_num > len(unlabeled_dictionary.keys()):
                    trajectory_labels = list(unlabeled_dictionary.keys())
                else:
                    trajectory_labels = random.sample(unlabeled_dictionary.keys(), k = k_num)
                trajectory_unlabeled_x = []
                
                for label in trajectory_labels:
                    trajectory_unlabeled_x.append(embedding_dict[label])
                trajectory_unlabeled_x = np.array(trajectory_unlabeled_x)

                x_train_full, y_train_full, trained_scaler = preprocess_data(labeled_dictionary, embedding_dict,\
                    mode = 'labeled')
                state = get_all_states_ablate(x_train_full, y_train_full, trajectory_unlabeled_x, model, ablate = 0)
                temp_states = np.expand_dims(state, axis = 0)
                policy_shape = policy.input_shape[1]
                n_batches = 0
                if policy_shape != k_num:
                    assert (k_num%policy_shape == 0), 'k_num in not divisible by the policy\'s expected input shape'
                    n_batches = k_num//policy_shape
                
                if n_batches:
                    best_per_batch = []
                    #slice and roll thorough. aggregate predicted best from each batch
                    for batch_number in range(n_batches):
                        start_ind = batch_number * policy_shape
                        stop_ind = (batch_number+1) * policy_shape
                        temp_states_subset = temp_states[:, start_ind:stop_ind, :]
                        action_subset = policy.predict(temp_states_subset)
                        best_action_subset = np.argmax(action_subset)
                        best_ind = best_action_subset + start_ind
                        best_per_batch.append(best_ind)
                        #this is the index of the best, with respect to the whole pool
                    #now take all of the best
                    mega_batch = np.empty([1,policy_shape,258])
                    meta_label = []
                    mega_batch_length = policy_shape
                    row_number = 0
                    for bn, bb in enumerate(best_per_batch):
                        copies = mega_batch_length // (n_batches - bn)
                        best = temp_states.take(bb, axis = 1)
                        for c in range(copies):
                            meta_label.append(trajectory_labels[bb])
                            mega_batch[:, row_number, :] = best
                            row_number +=1
                        mega_batch_length -= copies
                        
                    all_actions = policy.predict(mega_batch)
                    action = np.argmax(all_actions)
                    selected_label = meta_label[action]

                else:
                    all_actions = policy.predict(temp_states)
                    action = np.argmax(all_actions)
                    selected_label = trajectory_labels[action]

                #adding the data...
                labeled_dictionary[selected_label] = ground_truth_dict[selected_label]
                del unlabeled_dictionary[selected_label]
                
            model.set_weights(initial_weights)
            x_train, y_train, scaler = preprocess_data(labeled_dictionary, embedding_dict,\
                mode = 'labeled')

            history = model.fit(np.array(x_train), y_train, epochs = 100, verbose = 0)
            loss = history.history['loss'][-1]
            loss_log.append(loss)
            #now predict the rest
            x_eval, y_eval = preprocess_data(unlabeled_dictionary, embedding_dict, mode = 'eval')
            predictions = model.predict(x_eval)
            log_ALIL.append([rep, t, loss_log, list(labeled_dictionary.values()), predictions, y_eval])
    del model
    K.clear_session()
    return log_ALIL
